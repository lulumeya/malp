/*
 *  Copyright (C) 2020 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/gateship-one/malp/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.gateshipone.malp.application.activities

import org.gateshipone.malp.application.activities.GenericActivity
import android.os.Bundle
import org.gateshipone.malp.R
import android.widget.TextView
import android.content.Intent
import org.gateshipone.malp.application.activities.ContributorsActivity
import android.content.ActivityNotFoundException
import android.net.Uri
import android.view.View
import org.gateshipone.malp.BuildConfig
import org.gateshipone.malp.application.fragments.ErrorDialog
import org.gateshipone.malp.application.fragments.LicensesDialog
import org.gateshipone.malp.application.utils.ThemeUtils
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDException.MPDServerException
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDException.MPDConnectionException

class AboutActivity : GenericActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        window.statusBarColor = ThemeUtils.getThemeColor(this, R.attr.malp_color_primary_dark)
        val versionName = BuildConfig.VERSION_NAME
        (findViewById<View>(R.id.activity_about_version) as TextView).text = versionName
        val gitHash = BuildConfig.GIT_COMMIT_HASH
        (findViewById<View>(R.id.activity_about_git_hash) as TextView).text = gitHash
        findViewById<View>(R.id.button_contributors).setOnClickListener { view: View? ->
            val myIntent = Intent(this@AboutActivity, ContributorsActivity::class.java)
            startActivity(myIntent)
        }
        findViewById<View>(R.id.logo_musicbrainz).setOnClickListener { view: View? ->
            val urlIntent = Intent(Intent.ACTION_VIEW)
            urlIntent.data = Uri.parse(resources.getString(R.string.url_musicbrainz))
            try {
                startActivity(urlIntent)
            } catch (e: ActivityNotFoundException) {
                val noBrowserFoundDlg = ErrorDialog.newInstance(R.string.dialog_no_browser_found_title, R.string.dialog_no_browser_found_message)
                noBrowserFoundDlg.show(supportFragmentManager, "BrowserNotFoundDlg")
            }
        }
        findViewById<View>(R.id.logo_lastfm).setOnClickListener { view: View? ->
            val urlIntent = Intent(Intent.ACTION_VIEW)
            urlIntent.data = Uri.parse(resources.getString(R.string.url_lastfm))
            try {
                startActivity(urlIntent)
            } catch (e: ActivityNotFoundException) {
                val noBrowserFoundDlg = ErrorDialog.newInstance(R.string.dialog_no_browser_found_title, R.string.dialog_no_browser_found_message)
                noBrowserFoundDlg.show(supportFragmentManager, "BrowserNotFoundDlg")
            }
        }
        findViewById<View>(R.id.logo_fanarttv).setOnClickListener { view: View? ->
            val urlIntent = Intent(Intent.ACTION_VIEW)
            urlIntent.data = Uri.parse(resources.getString(R.string.url_fanarttv))
            try {
                startActivity(urlIntent)
            } catch (e: ActivityNotFoundException) {
                val noBrowserFoundDlg = ErrorDialog.newInstance(R.string.dialog_no_browser_found_title, R.string.dialog_no_browser_found_message)
                noBrowserFoundDlg.show(supportFragmentManager, "BrowserNotFoundDlg")
            }
        }
        findViewById<View>(R.id.thirdparty_licenses).setOnClickListener { view: View? -> LicensesDialog.newInstance().show(supportFragmentManager, LicensesDialog::class.java.simpleName) }
    }

    override fun onConnected() {}
    override fun onDisconnected() {}
    override fun onMPDError(e: MPDServerException?) {}
    override fun onMPDConnectionError(e: MPDConnectionException?) {}
}