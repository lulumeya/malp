/*
 *  Copyright (C) 2020 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/gateship-one/malp/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.gateshipone.malp.application.utils

import android.content.Context
import org.gateshipone.malp.application.utils.CoverBitmapLoader.CoverBitmapListener
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDTrack
import org.gateshipone.malp.application.utils.CoverBitmapLoader.ImageRunner
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDArtist
import org.gateshipone.malp.application.utils.CoverBitmapLoader.ArtistImageRunner
import org.gateshipone.malp.application.utils.CoverBitmapLoader.TrackArtistImageRunner
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDAlbum
import org.gateshipone.malp.application.utils.CoverBitmapLoader.AlbumImageRunner
import android.graphics.Bitmap
import org.gateshipone.malp.application.artwork.BitmapCache
import org.gateshipone.malp.application.utils.CoverBitmapLoader.IMAGE_TYPE
import org.gateshipone.malp.application.artwork.ArtworkManager
import org.gateshipone.malp.application.artwork.storage.ImageNotFoundException
import org.gateshipone.malp.application.utils.CoverBitmapLoader

class CoverBitmapLoader(context: Context, listener: CoverBitmapListener) {
    private val mListener: CoverBitmapListener
    private val mApplicationContext: Context

    /**
     * Enum to define the type of the image that was retrieved
     */
    enum class IMAGE_TYPE {
        ALBUM_IMAGE, ARTIST_IMAGE
    }

    /**
     * Load the image for the given track from the mediastore.
     */
    fun getImage(track: MPDTrack?, fetchImage: Boolean, width: Int, height: Int) {
        if (track != null) {
            // start the loader thread to load the image async
            val loaderThread = Thread(ImageRunner(fetchImage, track, width, height))
            loaderThread.start()
        }
    }

    fun getArtistImage(artist: MPDArtist?, fetchImage: Boolean, width: Int, height: Int) {
        if (artist == null) {
            return
        }

        // start the loader thread to load the image async
        val loaderThread = Thread(ArtistImageRunner(artist, fetchImage, width, height))
        loaderThread.start()
    }

    fun getArtistImage(track: MPDTrack?, fetchImage: Boolean, width: Int, height: Int) {
        if (track == null) {
            return
        }

        // start the loader thread to load the image async
        val loaderThread = Thread(TrackArtistImageRunner(track, fetchImage, width, height))
        loaderThread.start()
    }

    fun getAlbumImage(album: MPDAlbum?, fetchImage: Boolean, width: Int, height: Int) {
        if (album == null) {
            return
        }

        // start the loader thread to load the image async
        val loaderThread = Thread(AlbumImageRunner(album, fetchImage, width, height))
        loaderThread.start()
    }

    private inner class ImageRunner(private val mFetchImage: Boolean, private val mTrack: MPDTrack, private val mWidth: Int, private val mHeight: Int) : Runnable {
        /**
         * Load the image for the given track from the mediastore.
         */
        override fun run() {
            val tempAlbum = mTrack.album

            // At first get image independent of resolution (can be replaced later with higher resolution)
            val image = BitmapCache.getInstance().requestAlbumBitmap(tempAlbum)
            if (image != null) {
                mListener.receiveBitmap(image, IMAGE_TYPE.ALBUM_IMAGE)
            }
            try {
                // If image was to small get it in the right resolution
                if (image == null || !(mWidth <= image.width && mHeight <= image.height)) {
                    val albumImage = ArtworkManager.getInstance(mApplicationContext).getImage(mTrack, mWidth, mHeight, true)
                    mListener.receiveBitmap(albumImage, IMAGE_TYPE.ALBUM_IMAGE)
                }
            } catch (e: ImageNotFoundException) {
                if (mFetchImage) {
                    ArtworkManager.getInstance(mApplicationContext).fetchImage(mTrack)
                }
            }
        }
    }

    private inner class ArtistImageRunner(private val mArtist: MPDArtist, private val mFetchImage: Boolean, private val mWidth: Int, private val mHeight: Int) : Runnable {
        /**
         * Load the image for the given track from the mediastore.
         */
        override fun run() {
            // At first get image independent of resolution (can be replaced later with higher resolution)
            var image = BitmapCache.getInstance().requestArtistImage(mArtist)
            if (image != null) {
                mListener.receiveBitmap(image, IMAGE_TYPE.ARTIST_IMAGE)
            }
            try {
                // If image was to small get it in the right resolution
                if (image == null || !(mWidth <= image.width && mHeight <= image.height)) {
                    image = ArtworkManager.getInstance(mApplicationContext).getImage(mArtist, mWidth, mHeight, true)
                    mListener.receiveBitmap(image, IMAGE_TYPE.ARTIST_IMAGE)
                }
            } catch (e: ImageNotFoundException) {
                if (mFetchImage) {
                    ArtworkManager.getInstance(mApplicationContext).fetchImage(mArtist)
                }
            }
        }
    }

    private inner class TrackArtistImageRunner(track: MPDTrack, fetchImage: Boolean, width: Int, height: Int) : Runnable {
        private val mWidth: Int
        private val mHeight: Int
        private val mArtist: MPDArtist
        private val mFetchImage: Boolean

        /**
         * Load the image for the given track from the mediastore.
         */
        override fun run() {
            // At first get image independent of resolution (can be replaced later with higher resolution)
            var image = BitmapCache.getInstance().requestArtistImage(mArtist)
            if (image != null) {
                mListener.receiveBitmap(image, IMAGE_TYPE.ARTIST_IMAGE)
            }
            try {
                // If image was to small get it in the right resolution
                if (image == null || !(mWidth <= image.width && mHeight <= image.height)) {
                    image = ArtworkManager.getInstance(mApplicationContext).getImage(mArtist, mWidth, mHeight, true)
                    mListener.receiveBitmap(image, IMAGE_TYPE.ARTIST_IMAGE)
                }
            } catch (e: ImageNotFoundException) {
                if (mFetchImage) {
                    ArtworkManager.getInstance(mApplicationContext).fetchImage(mArtist)
                }
            }
        }

        init {
            mArtist = MPDArtist(track.getStringTag(MPDTrack.StringTagTypes.ARTIST))
            if (!track.getStringTag(MPDTrack.StringTagTypes.ARTIST_MBID).isEmpty()) {
                mArtist.addMBID(track.getStringTag(MPDTrack.StringTagTypes.ARTIST_MBID))
            }
            mFetchImage = fetchImage
            mWidth = width
            mHeight = height
        }
    }

    private inner class AlbumImageRunner(private val mAlbum: MPDAlbum, private val mFetchImage: Boolean, private val mWidth: Int, private val mHeight: Int) : Runnable {
        /**
         * Load the image for the given track from the mediastore.
         */
        override fun run() {
            // At first get image independent of resolution (can be replaced later with higher resolution)
            val image = BitmapCache.getInstance().requestAlbumBitmap(mAlbum)
            if (image != null) {
                mListener.receiveBitmap(image, IMAGE_TYPE.ALBUM_IMAGE)
            }
            try {
                // If image was to small get it in the right resolution
                if (image == null || !(mWidth <= image.width && mHeight <= image.height)) {
                    val albumImage = ArtworkManager.getInstance(mApplicationContext).getImage(mAlbum, mWidth, mHeight, true)
                    mListener.receiveBitmap(albumImage, IMAGE_TYPE.ALBUM_IMAGE)
                }
            } catch (e: ImageNotFoundException) {
                if (mFetchImage) {
                    ArtworkManager.getInstance(mApplicationContext).fetchImage(mAlbum)
                }
            }
        }
    }

    /**
     * Callback if image was loaded.
     */
    interface CoverBitmapListener {
        fun receiveBitmap(bm: Bitmap?, type: IMAGE_TYPE?)
    }

    companion object {
        private val TAG = CoverBitmapLoader::class.java.simpleName
    }

    init {
        mApplicationContext = context.applicationContext
        mListener = listener
    }
}