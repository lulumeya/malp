/*
 *  Copyright (C) 2020 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/gateship-one/malp/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.gateshipone.malp.application.fragments.serverfragments

import android.content.Context
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDAlbum
import org.gateshipone.malp.application.listviewitems.GenericViewItemHolder
import org.gateshipone.malp.application.utils.CoverBitmapLoader.CoverBitmapListener
import org.gateshipone.malp.application.artwork.ArtworkManager.onNewArtistImageListener
import org.gateshipone.malp.application.views.MalpRecyclerView
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDArtist
import org.gateshipone.malp.application.callbacks.AlbumCallback
import android.graphics.Bitmap
import org.gateshipone.malp.application.utils.CoverBitmapLoader
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDAlbum.MPD_ALBUM_SORT_ORDER
import android.os.Bundle
import org.gateshipone.malp.R
import android.preference.PreferenceManager
import android.view.*
import org.gateshipone.malp.application.adapters.AlbumsRecyclerViewAdapter
import org.gateshipone.malp.application.utils.RecyclerScrollSpeedListener
import org.gateshipone.malp.application.utils.PreferenceHelper
import org.gateshipone.malp.application.viewmodels.GenericViewModel
import androidx.lifecycle.ViewModelProvider
import org.gateshipone.malp.application.viewmodels.AlbumsViewModel.AlbumViewModelFactory
import org.gateshipone.malp.application.viewmodels.AlbumsViewModel
import org.gateshipone.malp.application.artwork.ArtworkManager
import android.view.ContextMenu.ContextMenuInfo
import org.gateshipone.malp.application.views.MalpRecyclerView.RecyclerViewContextMenuInfo
import androidx.core.graphics.drawable.DrawableCompat
import org.gateshipone.malp.application.listviewitems.AbsImageListViewItem
import org.gateshipone.malp.application.utils.CoverBitmapLoader.IMAGE_TYPE
import androidx.lifecycle.Observer
import org.gateshipone.malp.application.utils.ThemeUtils
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDQueryHandler
import java.lang.ClassCastException

class ArtistAlbumsFragment : GenericMPDRecyclerFragment<MPDAlbum?, GenericViewItemHolder?>(), CoverBitmapListener, onNewArtistImageListener, MalpRecyclerView.OnItemClickListener {
    private var mArtist: MPDArtist? = null
    private var mAlbumSelectCallback: AlbumCallback? = null
    private var mUseArtistSort = false
    private var mBitmap: Bitmap? = null
    private var mBitmapLoader: CoverBitmapLoader? = null
    private var mSortOrder: MPD_ALBUM_SORT_ORDER? = null

    /**
     * Save the last position here. Gets reused when the user returns to this view after selecting sme
     * albums.
     */
    private var mLastPosition = -1
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.recycler_list_refresh, container, false)
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val libraryView = sharedPref.getString(getString(R.string.pref_library_view_key), getString(R.string.pref_library_view_default))
        val useList = libraryView == getString(R.string.pref_library_view_list_key)
        mRecyclerView = rootView.findViewById(R.id.recycler_view)
        mAdapter = AlbumsRecyclerViewAdapter(requireContext().applicationContext, useList)
        if (useList) {
            mRecyclerView.adapter = mAdapter
            setLinearLayoutManagerAndDecoration()
        } else {
            mRecyclerView.adapter = mAdapter
            setGridLayoutManagerAndDecoration()
        }
        mRecyclerView.addOnScrollListener(RecyclerScrollSpeedListener(mAdapter))
        mRecyclerView.addOnItemClicklistener(this)
        registerForContextMenu(mRecyclerView)
        mSortOrder = PreferenceHelper.getMPDAlbumSortOrder(sharedPref, context)
        mUseArtistSort = sharedPref.getBoolean(getString(R.string.pref_use_artist_sort_key), resources.getBoolean(R.bool.pref_use_artist_sort_default))

        /* Check if an artistname was given in the extras */
        val args = arguments
        mArtist = args!!.getParcelable(BUNDLE_STRING_EXTRA_ARTIST)
        mBitmap = args.getParcelable(BUNDLE_STRING_EXTRA_BITMAP)
        setHasOptionsMenu(true)

        // get swipe layout
        mSwipeRefreshLayout = rootView.findViewById(R.id.refresh_layout)
        // set swipe colors
        mSwipeRefreshLayout.setColorSchemeColors(ThemeUtils.getThemeColor(context, R.attr.colorAccent),
                ThemeUtils.getThemeColor(context, R.attr.colorPrimary))
        // set swipe refresh listener
        mSwipeRefreshLayout.setOnRefreshListener { refreshContent() }
        mBitmapLoader = CoverBitmapLoader(requireContext(), this)
        viewModel.getData().observe(viewLifecycleOwner, { model: MutableList<MPDAlbum?>? -> this.onDataReady(model) })
        return rootView
    }

    public override fun getViewModel(): GenericViewModel<MPDAlbum?> {
        return ViewModelProvider(this, AlbumViewModelFactory(requireActivity().application, mArtist!!.artistName, null)).get(AlbumsViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        setupToolbarAndStuff()
        ArtworkManager.getInstance(context).registerOnNewArtistImageListener(this)
        ArtworkManager.getInstance(context).registerOnNewAlbumImageListener(mAdapter as AlbumsRecyclerViewAdapter)
    }

    /**
     * Called when the fragment is first attached to its context.
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        mAlbumSelectCallback = try {
            context as AlbumCallback
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement OnArtistSelectedListener")
        }
    }

    /**
     * Called when the observed [androidx.lifecycle.LiveData] is changed.
     *
     *
     * This method will update the related adapter and the [androidx.swiperefreshlayout.widget.SwipeRefreshLayout] if present.
     *
     * @param model The data observed by the [androidx.lifecycle.LiveData].
     */
    override fun onDataReady(model: MutableList<MPDAlbum?>?) {
        super.onDataReady(model)

        // Reset old scroll position
        if (mLastPosition >= 0) {
            mRecyclerView.layoutManager!!.scrollToPosition(mLastPosition)
            mLastPosition = -1
        }
    }

    override fun onPause() {
        super.onPause()
        ArtworkManager.getInstance(context).unregisterOnNewArtistImageListener(this)
        ArtworkManager.getInstance(context).unregisterOnNewAlbumImageListener(mAdapter as AlbumsRecyclerViewAdapter)
    }

    /**
     * Create the context menu.
     */
    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = requireActivity().menuInflater
        inflater.inflate(R.menu.context_menu_album, menu)
    }

    /**
     * Hook called when an menu item in the context menu is selected.
     *
     * @param item The menu item that was selected.
     * @return True if the hook was consumed here.
     */
    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as RecyclerViewContextMenuInfo
                ?: return super.onContextItemSelected(item)
        val itemId = item.itemId
        if (itemId == R.id.fragment_albums_action_enqueue) {
            enqueueAlbum(info.position)
            return true
        } else if (itemId == R.id.fragment_albums_action_play) {
            playAlbum(info.position)
            return true
        }
        return super.onContextItemSelected(item)
    }

    /**
     * Initialize the options menu.
     * Be sure to call [.setHasOptionsMenu] before.
     *
     * @param menu         The container for the custom options menu.
     * @param menuInflater The inflater to instantiate the layout.
     */
    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (null != mArtist && mArtist!!.artistName != "") {
            menuInflater.inflate(R.menu.fragment_menu_albums, menu)

            // get tint color
            val tintColor = ThemeUtils.getThemeColor(context, R.attr.malp_color_text_accent)
            var drawable = menu.findItem(R.id.action_add_artist).icon
            drawable = DrawableCompat.wrap(drawable!!)
            DrawableCompat.setTint(drawable, tintColor)
            menu.findItem(R.id.action_add_artist).icon = drawable
            menu.findItem(R.id.action_reset_artwork).isVisible = true
        }
        super.onCreateOptionsMenu(menu, menuInflater)
    }

    /**
     * Hook called when an menu item in the options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return True if the hook was consumed here.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId
        if (itemId == R.id.action_reset_artwork) {
            setupToolbarAndStuff()
            ArtworkManager.getInstance(context).resetArtistImage(mArtist)
            return true
        } else if (itemId == R.id.action_add_artist) {
            enqueueArtist()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        // Do not save the bitmap for later use (too big for binder)
        arguments?.remove(BUNDLE_STRING_EXTRA_BITMAP)
        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onItemClick(position: Int) {
        mLastPosition = position
        val album = mAdapter.getItem(position)!!
        var bitmap: Bitmap? = null
        val view = mRecyclerView.getChildAt(position)

        // Check if correct view type, to be safe
        if (view is AbsImageListViewItem) {
            bitmap = view.bitmap
        }

        // If artist albums are shown set artist for the album (necessary for old MPD version, which don't
        // support group commands and therefore do not provide artist tags for albums)
        if (mArtist != null && !mArtist!!.artistName.isEmpty() && album.artistName.isEmpty()) {
            album.artistName = mArtist!!.artistName
            album.artistSortName = mArtist!!.artistName
        }

        // send the event to the host activity
        mAlbumSelectCallback!!.onAlbumSelected(album, bitmap)
    }

    override fun receiveBitmap(bm: Bitmap?, type: IMAGE_TYPE?) {
        if (type === IMAGE_TYPE.ARTIST_IMAGE && null != mFABCallback && bm != null) {
            val activity = activity
            activity?.runOnUiThread {
                mFABCallback.setupToolbar(mArtist!!.artistName, false, false, true)
                mFABCallback.setupToolbarImage(bm)
                arguments?.putParcelable(BUNDLE_STRING_EXTRA_BITMAP, bm)
            }
        }
    }

    private fun setupToolbarAndStuff() {
        if (null != mFABCallback) {
            if (null != mArtist && !mArtist!!.artistName.isEmpty()) {
                mFABCallback.setupFAB(true) { view: View? ->
                    if (mUseArtistSort) {
                        MPDQueryHandler.playArtistSort(mArtist!!.artistName, mSortOrder)
                    } else {
                        MPDQueryHandler.playArtist(mArtist!!.artistName, mSortOrder)
                    }
                }
                if (mBitmap == null) {
                    val rootView = view
                    rootView!!.post {
                        val size = rootView.width
                        mBitmapLoader!!.getArtistImage(mArtist, true, size, size)
                    }
                    mFABCallback.setupToolbar(mArtist!!.artistName, false, false, false)
                } else {
                    // Reuse image
                    mFABCallback.setupToolbar(mArtist!!.artistName, false, false, true)
                    mFABCallback.setupToolbarImage(mBitmap)
                    val rootView = view
                    rootView!!.post {
                        val size = rootView.width

                        // Image too small
                        if (mBitmap!!.width < size) {
                            mBitmapLoader!!.getArtistImage(mArtist, true, size, size)
                        }
                    }
                }
            }
        }
    }

    /**
     * Callback for asynchronous image fetching
     *
     * @param artist Artist for which a new image is received
     */
    override fun newArtistImage(artist: MPDArtist) {
        if (artist == mArtist) {
            setupToolbarAndStuff()
        }
    }

    /**
     * Enqueues the album selected by the user
     *
     * @param index Index of the selected album
     */
    private fun enqueueAlbum(index: Int) {
        val album = mAdapter.getItem(index) as MPDAlbum

        // If artist albums are shown set artist for the album (necessary for old MPD version, which don't
        // support group commands and therefore do not provide artist tags for albums)
        if (mArtist != null && !mArtist!!.artistName.isEmpty() && album.artistName.isEmpty()) {
            album.artistName = mArtist!!.artistName
            album.artistSortName = mArtist!!.artistName
        }
        MPDQueryHandler.addArtistAlbum(album.name, album.artistName, album.mbid)
    }

    /**
     * Plays the album selected by the user
     *
     * @param index Index of the selected album
     */
    private fun playAlbum(index: Int) {
        val album = mAdapter.getItem(index) as MPDAlbum

        // If artist albums are shown set artist for the album (necessary for old MPD version, which don't
        // support group commands and therefore do not provide artist tags for albums)
        if (mArtist != null && !mArtist!!.artistName.isEmpty() && album.artistName.isEmpty()) {
            album.artistName = mArtist!!.artistName
            album.artistSortName = mArtist!!.artistName
        }
        MPDQueryHandler.playArtistAlbum(album.name, album.artistName, album.mbid)
    }

    /**
     * Enqueues the artist that is currently shown (if the fragment is not shown for all albums)
     */
    private fun enqueueArtist() {
        MPDQueryHandler.addArtist(mArtist!!.artistName, mSortOrder)
    }

    companion object {
        val TAG = ArtistAlbumsFragment::class.java.simpleName

        /**
         * Definition of bundled extras
         */
        private const val BUNDLE_STRING_EXTRA_ARTIST = "artist"
        private const val BUNDLE_STRING_EXTRA_BITMAP = "bitmap"
        fun newInstance(artist: MPDArtist, bitmap: Bitmap?): ArtistAlbumsFragment {
            val args = Bundle()
            args.putParcelable(BUNDLE_STRING_EXTRA_ARTIST, artist)
            args.putParcelable(BUNDLE_STRING_EXTRA_BITMAP, bitmap)
            val fragment = ArtistAlbumsFragment()
            fragment.arguments = args
            return fragment
        }
    }
}