package org.gateshipone.malp.application.retrofit

import com.facebook.flipper.plugins.network.NetworkFlipperPlugin

object FlipperHelper {
    val networkFlipperPlugin = NetworkFlipperPlugin()
}