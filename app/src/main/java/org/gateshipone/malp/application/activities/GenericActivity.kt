/*
 *  Copyright (C) 2020 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/gateship-one/malp/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.gateshipone.malp.application.activities

import android.content.*
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import android.os.Looper
import android.os.RemoteException
import android.preference.PreferenceManager
import android.view.KeyEvent
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import org.gateshipone.malp.R
import org.gateshipone.malp.application.activities.GenericActivity
import org.gateshipone.malp.application.background.BackgroundService
import org.gateshipone.malp.application.background.BackgroundService.STREAMING_STATUS
import org.gateshipone.malp.application.background.BackgroundServiceConnection
import org.gateshipone.malp.application.background.BackgroundServiceConnection.OnConnectionStatusChangedListener
import org.gateshipone.malp.application.utils.HardwareKeyHandler
import org.gateshipone.malp.mpdservice.ConnectionManager
import org.gateshipone.malp.mpdservice.handlers.MPDConnectionErrorHandler
import org.gateshipone.malp.mpdservice.handlers.MPDConnectionStateChangeHandler
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDCommandHandler
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDQueryHandler
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDStateMonitoringHandler
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDException.MPDConnectionException
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDException.MPDServerException
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDInterface
import java.lang.ref.WeakReference

abstract class GenericActivity : AppCompatActivity(), OnSharedPreferenceChangeListener {
    private var mHardwareControls = false
    private var mKeepDisplayOn = false
    private var mBackgroundServiceConnection: BackgroundServiceConnection? = null
    private var mStreamingStatus: STREAMING_STATUS? = null
    private var mConnectionCallback: MPDConnectionStateCallbackHandler? = null
    private var mStreamingStatusReceiver: StreamingStatusReceiver? = null
    private var mErrorListener: MPDErrorListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Read theme preference
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val themePref = sharedPref.getString(getString(R.string.pref_theme_key), getString(R.string.pref_theme_default))
        val darkTheme = sharedPref.getBoolean(getString(R.string.pref_dark_theme_key), resources.getBoolean(R.bool.pref_theme_dark_default))
        if (darkTheme) {
            if (themePref == getString(R.string.pref_indigo_key)) {
                setTheme(R.style.AppTheme_indigo)
            } else if (themePref == getString(R.string.pref_orange_key)) {
                setTheme(R.style.AppTheme_orange)
            } else if (themePref == getString(R.string.pref_deeporange_key)) {
                setTheme(R.style.AppTheme_deepOrange)
            } else if (themePref == getString(R.string.pref_blue_key)) {
                setTheme(R.style.AppTheme_blue)
            } else if (themePref == getString(R.string.pref_darkgrey_key)) {
                setTheme(R.style.AppTheme_darkGrey)
            } else if (themePref == getString(R.string.pref_brown_key)) {
                setTheme(R.style.AppTheme_brown)
            } else if (themePref == getString(R.string.pref_lightgreen_key)) {
                setTheme(R.style.AppTheme_lightGreen)
            } else if (themePref == getString(R.string.pref_red_key)) {
                setTheme(R.style.AppTheme_red)
            }
        } else {
            if (themePref == getString(R.string.pref_indigo_key)) {
                setTheme(R.style.AppTheme_indigo_light)
            } else if (themePref == getString(R.string.pref_orange_key)) {
                setTheme(R.style.AppTheme_orange_light)
            } else if (themePref == getString(R.string.pref_deeporange_key)) {
                setTheme(R.style.AppTheme_deepOrange_light)
            } else if (themePref == getString(R.string.pref_blue_key)) {
                setTheme(R.style.AppTheme_blue_light)
            } else if (themePref == getString(R.string.pref_darkgrey_key)) {
                setTheme(R.style.AppTheme_darkGrey_light)
            } else if (themePref == getString(R.string.pref_brown_key)) {
                setTheme(R.style.AppTheme_brown_light)
            } else if (themePref == getString(R.string.pref_lightgreen_key)) {
                setTheme(R.style.AppTheme_lightGreen_light)
            } else if (themePref == getString(R.string.pref_red_key)) {
                setTheme(R.style.AppTheme_red_light)
            }
        }
        if (themePref == getString(R.string.pref_oleddark_key)) {
            setTheme(R.style.AppTheme_oledDark)
        }
        mErrorListener = MPDErrorListener(this)
    }

    override fun onResume() {
        super.onResume()

        // Add error listener
        MPDStateMonitoringHandler.getHandler().addErrorListener(mErrorListener)
        MPDCommandHandler.getHandler().addErrorListener(mErrorListener)
        MPDQueryHandler.getHandler().addErrorListener(mErrorListener)
        mConnectionCallback = MPDConnectionStateCallbackHandler(this, mainLooper)
        MPDInterface.getGenericInstance().addMPDConnectionStateChangeListener(mConnectionCallback)
        ConnectionManager.getInstance(applicationContext).registerMPDUse(applicationContext)
        if (null == mBackgroundServiceConnection) {
            mBackgroundServiceConnection = BackgroundServiceConnection(applicationContext, BackgroundServiceConnectionStateListener())
        }
        mBackgroundServiceConnection!!.openConnection()
        if (mStreamingStatusReceiver == null) {
            mStreamingStatusReceiver = StreamingStatusReceiver()
        }
        val filter = IntentFilter()
        filter.addAction(BackgroundService.ACTION_STREAMING_STATUS_CHANGED)
        applicationContext.registerReceiver(mStreamingStatusReceiver, filter)

        // Check if hardware key control is enabled by the user
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        sharedPref.registerOnSharedPreferenceChangeListener(this)
        mHardwareControls = sharedPref.getBoolean(getString(R.string.pref_hardware_controls_key), resources.getBoolean(R.bool.pref_hardware_controls_default))
        HardwareKeyHandler.getInstance().setVolumeStepSize(sharedPref.getInt(getString(R.string.pref_volume_steps_key), resources.getInteger(R.integer.pref_volume_steps_default)))
        mKeepDisplayOn = sharedPref.getBoolean(getString(R.string.pref_keep_display_on_key), resources.getBoolean(R.bool.pref_keep_display_on_default))
        handleKeepDisplayOnSetting()
    }

    override fun onPause() {
        super.onPause()
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        // Disconnect from MPD server
        ConnectionManager.getInstance(applicationContext).unregisterMPDUse(applicationContext)
        mBackgroundServiceConnection!!.closeConnection()
        sharedPref.unregisterOnSharedPreferenceChangeListener(this)
        MPDInterface.getGenericInstance().removeMPDConnectionStateChangeListener(mConnectionCallback)
        mConnectionCallback = null
        applicationContext.unregisterReceiver(mStreamingStatusReceiver)

        // Unregister error listeners
        MPDStateMonitoringHandler.getHandler().removeErrorListener(mErrorListener)
        MPDCommandHandler.getHandler().removeErrorListener(mErrorListener)
        MPDQueryHandler.getHandler().removeErrorListener(mErrorListener)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        if (key == getString(R.string.pref_hardware_controls_key)) {
            mHardwareControls = sharedPreferences.getBoolean(getString(R.string.pref_hardware_controls_key), resources.getBoolean(R.bool.pref_hardware_controls_default))
        } else if (key == getString(R.string.pref_volume_steps_key)) {
            // Set the hardware key handler to the new value
            HardwareKeyHandler.getInstance().setVolumeStepSize(sharedPreferences.getInt(getString(R.string.pref_volume_steps_key), resources.getInteger(R.integer.pref_volume_steps_default)))
        } else if (key == getString(R.string.pref_keep_display_on_key)) {
            mKeepDisplayOn = sharedPreferences.getBoolean(getString(R.string.pref_keep_display_on_key), resources.getBoolean(R.bool.pref_keep_display_on_default))
            handleKeepDisplayOnSetting()
        }
    }

    /**
     * Handles the volume keys of the device to control MPDs volume.
     *
     * @param event KeyEvent that was pressed by the user.
     * @return True if handled by MALP
     */
    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        val streamingActive = mStreamingStatus != STREAMING_STATUS.STOPPED
        return if (mHardwareControls) {
            HardwareKeyHandler.getInstance().handleKeyEvent(event, !streamingActive) || super.dispatchKeyEvent(event)
        } else {
            super.dispatchKeyEvent(event)
        }
    }

    private fun handleKeepDisplayOnSetting() {
        if (mKeepDisplayOn) {
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }

    protected abstract fun onConnected()
    protected abstract fun onDisconnected()
    protected abstract fun onMPDError(e: MPDServerException?)
    protected abstract fun onMPDConnectionError(e: MPDConnectionException?)
    private class MPDConnectionStateCallbackHandler internal constructor(activity: GenericActivity, looper: Looper?) : MPDConnectionStateChangeHandler(looper) {
        private val mActivity: WeakReference<GenericActivity>
        override fun onConnected() {
            mActivity.get()!!.onConnected()
        }

        override fun onDisconnected() {
            mActivity.get()!!.onDisconnected()
        }

        init {
            mActivity = WeakReference(activity)
        }
    }

    /**
     * Receives stream playback status updates. When stream playback is started the status
     * is necessary to show the right menu item.
     */
    private inner class StreamingStatusReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == BackgroundService.ACTION_STREAMING_STATUS_CHANGED) {
                mStreamingStatus = STREAMING_STATUS.values()[intent.getIntExtra(BackgroundService.INTENT_EXTRA_STREAMING_STATUS, 0)]
            }
        }
    }

    /**
     * Private class to handle when a [android.content.ServiceConnection] to the [BackgroundService]
     * is established. When the connection is established, the stream playback status is retrieved.
     */
    private inner class BackgroundServiceConnectionStateListener : OnConnectionStatusChangedListener {
        override fun onConnected() {
            try {
                mStreamingStatus = STREAMING_STATUS.values()[mBackgroundServiceConnection!!.service.streamingStatus]
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }

        override fun onDisconnected() {}
    }

    private class MPDErrorListener(activity: GenericActivity) : MPDConnectionErrorHandler() {
        private val mActivity: WeakReference<GenericActivity>
        override fun onMPDError(e: MPDServerException) {
            mActivity.get()!!.onMPDError(e)
        }

        override fun onMPDConnectionError(e: MPDConnectionException) {
            mActivity.get()!!.onMPDConnectionError(e)
        }

        init {
            mActivity = WeakReference(activity)
        }
    }

    companion object {
        private val TAG = GenericActivity::class.java.simpleName
    }
}