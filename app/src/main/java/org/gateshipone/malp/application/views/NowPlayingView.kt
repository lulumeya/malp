/*
 *  Copyright (C) 2020 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/gateship-one/malp/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.gateshipone.malp.application.views

import android.app.Activity
import android.content.*
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.os.RemoteException
import android.preference.PreferenceManager
import android.util.AttributeSet
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.View.OnClickListener
import android.view.View.OnLongClickListener
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.TooltipCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.ViewCompat
import androidx.customview.widget.ViewDragHelper
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.delay
import org.gateshipone.malp.R
import org.gateshipone.malp.application.activities.FanartActivity
import org.gateshipone.malp.application.artwork.ArtworkManager
import org.gateshipone.malp.application.artwork.ArtworkManager.onNewAlbumImageListener
import org.gateshipone.malp.application.artwork.ArtworkManager.onNewArtistImageListener
import org.gateshipone.malp.application.background.BackgroundService
import org.gateshipone.malp.application.background.BackgroundService.STREAMING_STATUS
import org.gateshipone.malp.application.background.BackgroundServiceConnection
import org.gateshipone.malp.application.background.BackgroundServiceConnection.OnConnectionStatusChangedListener
import org.gateshipone.malp.application.callbacks.OnSaveDialogListener
import org.gateshipone.malp.application.fragments.ErrorDialog
import org.gateshipone.malp.application.fragments.TextDialog
import org.gateshipone.malp.application.fragments.serverfragments.ChoosePlaylistDialog
import org.gateshipone.malp.application.retrofit.Retro
import org.gateshipone.malp.application.utils.*
import org.gateshipone.malp.application.utils.CoverBitmapLoader.CoverBitmapListener
import org.gateshipone.malp.application.utils.CoverBitmapLoader.IMAGE_TYPE
import org.gateshipone.malp.application.views.NowPlayingView.NowPlayingDragStatusReceiver.DRAG_STATUS
import org.gateshipone.malp.application.views.NowPlayingView.NowPlayingDragStatusReceiver.VIEW_SWITCHER_STATUS
import org.gateshipone.malp.mpdservice.ConnectionManager
import org.gateshipone.malp.mpdservice.handlers.MPDConnectionStateChangeHandler
import org.gateshipone.malp.mpdservice.handlers.MPDStatusChangeHandler
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDCommandHandler
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDQueryHandler
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDStateMonitoringHandler
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDInterface
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDAlbum
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDArtist
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDCurrentStatus
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDCurrentStatus.MPD_PLAYBACK_STATE
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDTrack
import java.lang.ref.WeakReference
import java.util.*

@Suppress("BlockingMethodInNonBlockingContext")
class NowPlayingView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RelativeLayout(context, attrs, defStyle), PopupMenu.OnMenuItemClickListener,
    onNewAlbumImageListener, onNewArtistImageListener, OnSharedPreferenceChangeListener {
    private val mDragHelper: ViewDragHelper
    private val mStateListener: ServerStatusListener
    private val mConnectionStateListener: ServerConnectionListener

    /**
     * Upper view part which is dragged up & down
     */
    private var mHeaderView: View? = null

    /**
     * Main view of draggable part
     */
    private var mMainView: View? = null
    private lateinit var mDraggedUpButtons: LinearLayout
    private lateinit var mDraggedDownButtons: LinearLayout

    /**
     * Absolute pixel position of upper layout bound
     */
    private var mTopPosition = 0

    /**
     * relative dragposition
     */
    private var mDragOffset = 0f

    /**
     * Height of non-draggable part.
     * (Layout height - draggable part)
     */
    private var mDragRange = 0

    /**
     * Flag whether the nowplaying hint should be shown. This should only be true if the app was used the first time.
     */
    private var mShowNPVHint = false

    /**
     * Flag whether the views switches between album cover and artist image
     */
    private var mShowArtistImage = false
    private var mStreamingStatus: STREAMING_STATUS? = null

    /**
     * Main cover imageview
     */
    private lateinit var mCoverImage: AlbumArtistView

    /**
     * Small cover image, part of the draggable header
     */
    private var mTopCoverImage: ImageView? = null

    /**
     * View that contains the playlist ListVIew
     */
    private lateinit var mPlaylistView: CurrentPlaylistView

    /**
     * ViewSwitcher used for switching between the main cover image and the playlist
     */
    private lateinit var mViewSwitcher: ViewSwitcher

    /**
     * Asynchronous loader for coverimages for TrackItems.
     */
    private var mCoverLoader: CoverBitmapLoader? = null

    /**
     * Observer for information about the state of the draggable part of this view.
     * This is probably the Activity of which this view is part of.
     * (Used for smooth statusbar transition and state resuming)
     */
    private var mDragStatusReceiver: NowPlayingDragStatusReceiver? = null
    private var mStreamingStatusReceiver: StreamingStatusReceiver? = null
    private var mBackgroundServiceConnection: BackgroundServiceConnection? = null

    /**
     * Top buttons in the draggable header part.
     */
    private lateinit var mTopPlayPauseButton: ImageButton
    private lateinit var mTopPlaylistButton: ImageButton
    private lateinit var mTopMenuButton: ImageButton

    /**
     * Buttons in the bottom part of the view
     */
    private lateinit var mBottomRepeatButton: ImageButton
    private lateinit var mBottomPreviousButton: ImageButton
    private lateinit var mBottomPlayPauseButton: ImageButton
    private lateinit var mBottomStopButton: ImageButton
    private lateinit var mBottomNextButton: ImageButton
    private lateinit var mBottomRandomButton: ImageButton

    /**
     * Seekbar used for seeking and informing the user of the current playback position.
     */
    private lateinit var mPositionSeekbar: SeekBar

    /**
     * Seekbar used for volume control of host
     */
    private lateinit var mVolumeSeekbar: SeekBar
    private lateinit var mVolumeIcon: ImageView
    private lateinit var mVolumeIconButtons: ImageView
    private var mVolumeText: TextView? = null
    private lateinit var mVolumeMinus: ImageButton
    private lateinit var mVolumePlus: ImageButton
    private var mPlusListener: VolumeButtonLongClickListener? = null
    private var mMinusListener: VolumeButtonLongClickListener? = null
    private var mHeaderTextLayout: LinearLayout? = null
    private var mVolumeSeekbarLayout: LinearLayout? = null
    private var mVolumeButtonLayout: LinearLayout? = null
    private var mVolumeStepSize = 0

    /**
     * Various textviews for track information
     */
    private lateinit var mTrackName: TextView
    private lateinit var mTrackAdditionalInfo: TextView
    private var mElapsedTime: TextView? = null
    private var mDuration: TextView? = null
    private var mTrackNo: TextView? = null
    private var mPlaylistNo: TextView? = null
    private var mBitrate: TextView? = null
    private var mAudioProperties: TextView? = null
    private var mTrackURI: TextView? = null
    private var mLastStatus: MPDCurrentStatus?
    private var mLastTrack: MPDTrack?
    private var mUseEnglishWikipedia = false

    /**
     * Maximizes this view with an animation.
     */
    fun maximize() {
        smoothSlideTo(0f)
    }

    /**
     * Minimizes the view with an animation.
     */
    fun minimize() {
        smoothSlideTo(1f)
    }

    /**
     * Slides the view to the given position.
     *
     * @param slideOffset 0.0 - 1.0 (0.0 is dragged down, 1.0 is dragged up)
     * @return If the move was successful
     */
    fun smoothSlideTo(slideOffset: Float): Boolean {
        val topBound = paddingTop
        val y = (topBound + slideOffset * mDragRange).toInt()
        if (mDragHelper.smoothSlideViewTo(mHeaderView!!, mHeaderView!!.left, y)) {
            ViewCompat.postInvalidateOnAnimation(this)
            return true
        }
        return false
    }

    /**
     * Set the position of the draggable view to the given offset. This is done without an animation.
     * Can be used to resume a certain state of the view (e.g. on resuming an activity)
     *
     * @param offset Offset to position the view to from 0.0 - 1.0 (0.0 dragged up, 1.0 dragged down)
     */
    fun setDragOffset(offset: Float) {
        mDragOffset = if (offset > 1.0f || offset < 0.0f) {
            1.0f
        } else {
            offset
        }
        invalidate()
        requestLayout()


        // Set inverse alpha values for smooth layout transition.
        // Visibility still needs to be set otherwise parts of the buttons
        // are not clickable.
        mDraggedDownButtons!!.alpha = mDragOffset
        mDraggedUpButtons!!.alpha = 1.0f - mDragOffset

        // Calculate the margin to smoothly resize text field
        val layoutParams = mHeaderTextLayout!!.layoutParams as LayoutParams
        layoutParams.marginEnd = (mTopPlaylistButton!!.width * (1.0 - mDragOffset)).toInt()
        mHeaderTextLayout!!.layoutParams = layoutParams

        // Notify the observers about the change
        if (mDragStatusReceiver != null) {
            mDragStatusReceiver!!.onDragPositionChanged(offset)
        }
        if (mDragOffset == 0.0f) {
            // top
            mDraggedDownButtons!!.visibility = INVISIBLE
            mDraggedUpButtons!!.visibility = VISIBLE
            mCoverImage!!.visibility = VISIBLE
            if (mDragStatusReceiver != null) {
                mDragStatusReceiver!!.onStatusChanged(DRAG_STATUS.DRAGGED_UP)
            }
        } else {
            // bottom
            mDraggedDownButtons!!.visibility = VISIBLE
            mDraggedUpButtons!!.visibility = INVISIBLE
            mCoverImage!!.visibility = INVISIBLE
            if (mDragStatusReceiver != null) {
                mDragStatusReceiver!!.onStatusChanged(DRAG_STATUS.DRAGGED_DOWN)
            }
        }
    }

    /**
     * Menu click listener. This method gets called when the user selects an item of the popup menu (right top corner).
     *
     * @param item MenuItem that was clicked.
     * @return Returns true if the item was handled by this method. False otherwise.
     */
    override fun onMenuItemClick(item: MenuItem): Boolean {
        val itemId = item.itemId
        if (itemId == R.id.action_clear_playlist) {
            val removeListBuilder = AlertDialog.Builder(context)
            removeListBuilder.setTitle(context.getString(R.string.action_delete_playlist))
            removeListBuilder.setMessage(context.getString(R.string.dialog_message_delete_current_playlist))
            removeListBuilder.setPositiveButton(R.string.dialog_action_yes) { dialog: DialogInterface?, which: Int -> MPDQueryHandler.clearPlaylist() }
            removeListBuilder.setNegativeButton(R.string.dialog_action_no) { dialog: DialogInterface?, which: Int -> }
            removeListBuilder.create().show()
        } else if (itemId == R.id.action_shuffle_playlist) {
            val shuffleListBuilder = AlertDialog.Builder(context)
            shuffleListBuilder.setTitle(context.getString(R.string.action_shuffle_playlist))
            shuffleListBuilder.setMessage(context.getString(R.string.dialog_message_shuffle_current_playlist))
            shuffleListBuilder.setPositiveButton(R.string.dialog_action_yes) { dialog: DialogInterface?, which: Int -> MPDQueryHandler.shufflePlaylist() }
            shuffleListBuilder.setNegativeButton(R.string.dialog_action_no) { dialog: DialogInterface?, which: Int -> }
            shuffleListBuilder.create().show()
        } else if (itemId == R.id.action_save_playlist) {
            val plDialogCallback: OnSaveDialogListener = object : OnSaveDialogListener {
                override fun onSaveObject(title: String) {
                    val overWriteBuilder = AlertDialog.Builder(context)
                    overWriteBuilder.setTitle(context.getString(R.string.action_overwrite_playlist))
                    overWriteBuilder.setMessage(context.getString(R.string.dialog_message_overwrite_playlist) + ' ' + title + '?')
                    overWriteBuilder.setPositiveButton(R.string.dialog_action_yes) { dialog: DialogInterface?, which: Int ->
                        MPDQueryHandler.removePlaylist(title)
                        MPDQueryHandler.savePlaylist(title)
                    }
                    overWriteBuilder.setNegativeButton(R.string.dialog_action_no) { dialog: DialogInterface?, which: Int -> }
                    overWriteBuilder.create().show()
                }

                override fun onCreateNewObject() {
                    // open dialog in order to save the current playlist as a playlist in the mediastore
                    val textDialog = TextDialog.newInstance(
                        resources.getString(R.string.dialog_save_playlist),
                        resources.getString(R.string.default_playlist_title)
                    )
                    textDialog.setCallback { name: String? -> MPDQueryHandler.savePlaylist(name) }
                    textDialog.show(
                        (context as AppCompatActivity).supportFragmentManager,
                        "SavePLTextDialog"
                    )
                }
            }

            // open dialog in order to save the current playlist as a playlist in the mediastore
            val choosePlaylistDialog = ChoosePlaylistDialog.newInstance(true)
            choosePlaylistDialog.setCallback(plDialogCallback)
            choosePlaylistDialog.show(
                (context as AppCompatActivity).supportFragmentManager,
                "ChoosePlaylistDialog"
            )
        } else if (itemId == R.id.action_add_url) {
            val addURLDialog =
                TextDialog.newInstance(resources.getString(R.string.action_add_url), "http://...")
            addURLDialog.setCallback { url: String? -> MPDQueryHandler.addPath(url) }
            addURLDialog.show((context as AppCompatActivity).supportFragmentManager, "AddURLDialog")
        } else if (itemId == R.id.action_add_url_playlist) {
            val addURLDialog =
                TextDialog.newInstance(resources.getString(R.string.action_add_url), "http://...")
            addURLDialog.setCallback { name: String? -> MPDQueryHandler.loadPlaylist(name) }
            addURLDialog.show((context as AppCompatActivity).supportFragmentManager, "AddURLDialog")
        } else if (itemId == R.id.action_jump_to_current) {
            mPlaylistView!!.jumpToCurrentSong()
        } else if (itemId == R.id.action_toggle_single_mode) {
            if (null != mLastStatus) {
                if (mLastStatus!!.singlePlayback == 0) {
                    MPDCommandHandler.setSingle(true)
                } else {
                    MPDCommandHandler.setSingle(false)
                }
            }
        } else if (itemId == R.id.action_toggle_consume_mode) {
            if (null != mLastStatus) {
                if (mLastStatus!!.consume == 0) {
                    MPDCommandHandler.setConsume(true)
                } else {
                    MPDCommandHandler.setConsume(false)
                }
            }
        } else if (itemId == R.id.action_open_fanart) {
            val intent = Intent(context, FanartActivity::class.java)
            context.startActivity(intent)
            return true
        } else if (itemId == R.id.action_wikipedia_album) {
            val albumIntent = Intent(Intent.ACTION_VIEW)
            //albumIntent.setData(Uri.parse("https://" + Locale.getDefault().getLanguage() + ".wikipedia.org/wiki/index.php?search=" + mLastTrack.getTrackAlbum() + "&title=Special:Search&go=Go"));
            if (mUseEnglishWikipedia) {
                albumIntent.data =
                    Uri.parse("https://en.wikipedia.org/wiki/" + mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ALBUM))
            } else {
                albumIntent.data = Uri.parse(
                    "https://" + Locale.getDefault().language + ".wikipedia.org/wiki/" + mLastTrack!!.getStringTag(
                        MPDTrack.StringTagTypes.ALBUM
                    )
                )
            }
            try {
                context.startActivity(albumIntent)
            } catch (e: ActivityNotFoundException) {
                val noBrowserFoundDlg = ErrorDialog.newInstance(
                    R.string.dialog_no_browser_found_title,
                    R.string.dialog_no_browser_found_message
                )
                noBrowserFoundDlg.show(
                    (context as AppCompatActivity).supportFragmentManager,
                    "BrowserNotFoundDlg"
                )
            }
            return true
        } else if (itemId == R.id.action_wikipedia_artist) {
            val artistIntent = Intent(Intent.ACTION_VIEW)
            //artistIntent.setData(Uri.parse("https://" + Locale.getDefault().getLanguage() + ".wikipedia.org/wiki/index.php?search=" + mLastTrack.getTrackAlbumArtist() + "&title=Special:Search&go=Go"));
            if (mUseEnglishWikipedia) {
                artistIntent.data =
                    Uri.parse("https://en.wikipedia.org/wiki/" + mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ARTIST))
            } else {
                artistIntent.data = Uri.parse(
                    "https://" + Locale.getDefault().language + ".wikipedia.org/wiki/" + mLastTrack!!.getStringTag(
                        MPDTrack.StringTagTypes.ARTIST
                    )
                )
            }
            try {
                context.startActivity(artistIntent)
            } catch (e: ActivityNotFoundException) {
                val noBrowserFoundDlg = ErrorDialog.newInstance(
                    R.string.dialog_no_browser_found_title,
                    R.string.dialog_no_browser_found_message
                )
                noBrowserFoundDlg.show(
                    (context as AppCompatActivity).supportFragmentManager,
                    "BrowserNotFoundDlg"
                )
            }
            return true
        } else if (itemId == R.id.action_start_streaming) {
            if (mStreamingStatus == STREAMING_STATUS.PLAYING || mStreamingStatus == STREAMING_STATUS.BUFFERING) {
                try {
                    mBackgroundServiceConnection!!.service.stopStreamingPlayback()
                } catch (ignored: RemoteException) {
                }
            } else {
                try {
                    mBackgroundServiceConnection!!.service.startStreamingPlayback()
                } catch (ignored: RemoteException) {
                }
            }
            return true
        } else if (itemId == R.id.action_share_current_song) {
            shareCurrentTrack()
            return true
        }
        return false
    }

    override fun newAlbumImage(album: MPDAlbum) {
        if (mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ALBUM) == album.name) {
            mCoverLoader!!.getImage(mLastTrack, true, mCoverImage!!.width, mCoverImage!!.height)
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        if (key == context.getString(R.string.pref_volume_controls_key)) {
            setVolumeControlSetting()
        } else if (key == context.getString(R.string.pref_use_english_wikipedia_key)) {
            mUseEnglishWikipedia = sharedPreferences.getBoolean(
                key,
                context.resources.getBoolean(R.bool.pref_use_english_wikipedia_default)
            )
        } else if (key == context.getString(R.string.pref_show_npv_artist_image_key)) {
            mShowArtistImage = sharedPreferences.getBoolean(
                key,
                context.resources.getBoolean(R.bool.pref_show_npv_artist_image_default)
            )

            // Show artist image if artwork is requested
            if (mShowArtistImage) {
                mCoverLoader!!.getArtistImage(
                    mLastTrack,
                    true,
                    mCoverImage!!.width,
                    mCoverImage!!.height
                )
            } else {
                // Hide artist image
                mCoverImage!!.clearArtistImage()
            }
        } else if (key == context.getString(R.string.pref_volume_steps_key)) {
            setVolumeControlSetting()
        }
    }

    private fun setVolumeControlSetting() {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val volumeControlView = sharedPref.getString(
            context.getString(R.string.pref_volume_controls_key),
            context.getString(R.string.pref_volume_control_view_default)
        )
        val volLayout = findViewById<LinearLayout>(R.id.volume_control_layout)
        if (volumeControlView == context.getString(R.string.pref_volume_control_view_off_key)) {
            if (volLayout != null) {
                volLayout.visibility = GONE
            }
            mVolumeSeekbarLayout!!.visibility = GONE
            mVolumeButtonLayout!!.visibility = GONE
        } else if (volumeControlView == context.getString(R.string.pref_volume_control_view_seekbar_key)) {
            if (volLayout != null) {
                volLayout.visibility = VISIBLE
            }
            mVolumeSeekbarLayout!!.visibility = VISIBLE
            mVolumeButtonLayout!!.visibility = GONE
        } else if (volumeControlView == context.getString(R.string.pref_volume_control_view_buttons_key)) {
            if (volLayout != null) {
                volLayout.visibility = VISIBLE
            }
            mVolumeSeekbarLayout!!.visibility = GONE
            mVolumeButtonLayout!!.visibility = VISIBLE
        }
        mVolumeStepSize = sharedPref.getInt(
            context.getString(R.string.pref_volume_steps_key),
            resources.getInteger(R.integer.pref_volume_steps_default)
        )
        mPlusListener!!.setVolumeStepSize(mVolumeStepSize)
        mMinusListener!!.setVolumeStepSize(mVolumeStepSize)
    }

    override fun newArtistImage(artist: MPDArtist) {
        if (mShowArtistImage && mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ARTIST) == artist.artistName) {
            mCoverLoader!!.getArtistImage(artist, false, mCoverImage!!.width, mCoverImage!!.height)
        }
    }

    /**
     * Observer class for changes of the drag status.
     */
    private inner class BottomDragCallbackHelper : ViewDragHelper.Callback() {
        /**
         * Checks if a given child view should act as part of the drag. This is only true for the header
         * element of this View-class.
         *
         * @param child     Child that was touched by the user
         * @param pointerId Id of the pointer used for touching the view.
         * @return True if the view should be allowed to be used as dragging part, false otheriwse.
         */
        override fun tryCaptureView(child: View, pointerId: Int): Boolean {
            return child === mHeaderView
        }

        /**
         * Called if the position of the draggable view is changed. This rerequests the layout of the view.
         *
         * @param changedView The view that was changed.
         * @param left        Left position of the view (should stay constant in this case)
         * @param top         Top position of the view
         * @param dx          Dimension of the width
         * @param dy          Dimension of the height
         */
        override fun onViewPositionChanged(
            changedView: View,
            left: Int,
            top: Int,
            dx: Int,
            dy: Int
        ) {
            // Save the heighest top position of this view.
            mTopPosition = top

            // Calculate the new drag offset
            mDragOffset = top.toFloat() / mDragRange

            // Relayout this view
            requestLayout()

            // Set inverse alpha values for smooth layout transition.
            // Visibility still needs to be set otherwise parts of the buttons
            // are not clickable.
            mDraggedDownButtons!!.alpha = mDragOffset
            mDraggedUpButtons!!.alpha = 1.0f - mDragOffset

            // Calculate the margin to smoothly resize text field
            val layoutParams = mHeaderTextLayout!!.layoutParams as LayoutParams
            layoutParams.marginEnd = (mTopPlaylistButton!!.width * (1.0 - mDragOffset)).toInt()
            mHeaderTextLayout!!.layoutParams = layoutParams
            if (mDragStatusReceiver != null) {
                mDragStatusReceiver!!.onDragPositionChanged(mDragOffset)
            }
        }

        /**
         * Called if the user lifts the finger(release the view) with a velocity
         *
         * @param releasedChild View that was released
         * @param xvel          x position of the view
         * @param yvel          y position of the view
         */
        override fun onViewReleased(releasedChild: View, xvel: Float, yvel: Float) {
            var top = paddingTop
            if (yvel > 0 || yvel == 0f && mDragOffset > 0.5f) {
                top += mDragRange
            }
            // Snap the view to top/bottom position
            mDragHelper.settleCapturedViewAt(releasedChild.left, top)
            invalidate()
        }

        /**
         * Returns the range within a view is allowed to be dragged.
         *
         * @param child Child to get the dragrange for
         * @return Dragging range
         */
        override fun getViewVerticalDragRange(child: View): Int {
            return mDragRange
        }

        /**
         * Clamps (limits) the view during dragging to the top or bottom(plus header height)
         *
         * @param child Child that is being dragged
         * @param top   Top position of the dragged view
         * @param dy    Delta value of the height
         * @return The limited height value (or valid position inside the clamped range).
         */
        override fun clampViewPositionVertical(child: View, top: Int, dy: Int): Int {
            val topBound = paddingTop
            val bottomBound = height - mHeaderView!!.height - mHeaderView!!.paddingBottom
            return Math.min(Math.max(top, topBound), bottomBound)
        }

        /**
         * Called when the drag state changed. Informs observers that it is either dragged up or down.
         * Also sets the visibility of button groups in the header
         *
         * @param state New drag state
         */
        override fun onViewDragStateChanged(state: Int) {
            super.onViewDragStateChanged(state)

            // Check if the new state is the idle state. If then notify the observer (if one is registered)
            if (state == ViewDragHelper.STATE_IDLE) {
                // Enable scrolling of the text views
                mTrackName!!.isSelected = true
                mTrackAdditionalInfo!!.isSelected = true
                if (mDragOffset == 0.0f) {
                    // Called when dragged up
                    mDraggedDownButtons!!.visibility = INVISIBLE
                    mDraggedUpButtons!!.visibility = VISIBLE
                    if (mDragStatusReceiver != null) {
                        mDragStatusReceiver!!.onStatusChanged(DRAG_STATUS.DRAGGED_UP)
                    }
                } else {
                    // Called when dragged down
                    mDraggedDownButtons!!.visibility = VISIBLE
                    mDraggedUpButtons!!.visibility = INVISIBLE
                    mCoverImage!!.visibility = INVISIBLE
                    if (mDragStatusReceiver != null) {
                        mDragStatusReceiver!!.onStatusChanged(DRAG_STATUS.DRAGGED_DOWN)
                    }
                }
            } else if (state == ViewDragHelper.STATE_DRAGGING) {
                /*
                 * Show both layouts to enable a smooth transition via
                 * alpha values of the layouts.
                 */
                mDraggedDownButtons!!.visibility = VISIBLE
                mDraggedUpButtons!!.visibility = VISIBLE
                mCoverImage!!.visibility = VISIBLE
                // report the change of the view
                if (mDragStatusReceiver != null) {
                    // Disable scrolling of the text views
                    mTrackName!!.isSelected = false
                    mTrackAdditionalInfo!!.isSelected = false
                    mDragStatusReceiver!!.onStartDrag()
                    if (mViewSwitcher!!.currentView === mPlaylistView && mDragOffset == 1.0f) {
                        mPlaylistView!!.jumpToCurrentSong()
                    }
                }
            }
        }
    }

    /**
     * Informs the dragHelper about a scroll movement.
     */
    override fun computeScroll() {
        // Continues the movement of the View Drag Helper and sets the invalidation for this View
        // if the animation is not finished and needs continuation
        if (mDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this)
        }
    }

    /**
     * Handles touch inputs to some views, to make sure, the ViewDragHelper is called.
     *
     * @param ev Touch input event
     * @return True if handled by this view or false otherwise
     */
    override fun onTouchEvent(ev: MotionEvent): Boolean {
        // Call the drag helper
        mDragHelper.processTouchEvent(ev)

        // Get the position of the new touch event
        val x = ev.x
        val y = ev.y

        // Check if the position lies in the bounding box of the header view (which is draggable)
        val isHeaderViewUnder = mDragHelper.isViewUnder(mHeaderView, x.toInt(), y.toInt())

        // Check if drag is handled by the helper, or the header or mainview. If not notify the system that input is not yet handled.
        return isHeaderViewUnder && isViewHit(mHeaderView, x.toInt(), y.toInt()) || isViewHit(
            mMainView,
            x.toInt(),
            y.toInt()
        )
    }

    /**
     * Checks if an input to coordinates lay within a View
     *
     * @param view View to check with
     * @param x    x value of the input
     * @param y    y value of the input
     * @return
     */
    private fun isViewHit(view: View?, x: Int, y: Int): Boolean {
        val viewLocation = IntArray(2)
        view!!.getLocationOnScreen(viewLocation)
        val parentLocation = IntArray(2)
        getLocationOnScreen(parentLocation)
        val screenX = parentLocation[0] + x
        val screenY = parentLocation[1] + y
        return screenX >= viewLocation[0] && screenX < viewLocation[0] + view.width && screenY >= viewLocation[1] && screenY < viewLocation[1] + view.height
    }

    /**
     * Asks the ViewGroup about the size of all its children and paddings around.
     *
     * @param widthMeasureSpec  The width requirements for this view
     * @param heightMeasureSpec The height requirements for this view
     */
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        // FIXME check why super.onMeasure(widthMeasureSpec, heightMeasureSpec); causes
        // problems with scrolling header view.
        measureChildren(widthMeasureSpec, heightMeasureSpec)
        val maxWidth = MeasureSpec.getSize(widthMeasureSpec)
        val maxHeight = MeasureSpec.getSize(heightMeasureSpec)
        setMeasuredDimension(
            resolveSizeAndState(maxWidth, widthMeasureSpec, 0),
            resolveSizeAndState(maxHeight, heightMeasureSpec, 0)
        )
        val imageParams = mCoverImage!!.layoutParams
        imageParams.height = mViewSwitcher!!.height
        mCoverImage!!.layoutParams = imageParams
        mCoverImage!!.requestLayout()


        // Calculate the margin to smoothly resize text field
        val layoutParams = mHeaderTextLayout!!.layoutParams as LayoutParams
        layoutParams.marginEnd = (mTopPlaylistButton!!.measuredHeight * (1.0 - mDragOffset)).toInt()
        mHeaderTextLayout!!.layoutParams = layoutParams
    }

    /**
     * Called after the layout inflater is finished.
     * Sets all global view variables to the ones inflated.
     */
    override fun onFinishInflate() {
        super.onFinishInflate()

        // Get both main views (header and bottom part)
        var fm1Button = findViewById<View>(R.id.fm1Button)
        var fm1ButtonTop = findViewById<View>(R.id.fm1ButtonTop)

        val fmClick = OnClickListener { v: View ->
            (context as? LifecycleOwner)?.lifecycleScope?.launchWhenCreated {
                try {
                    val response = Retro.kbsService.getFm1UrlByKong()
                    MPDQueryHandler.addPathAtIndex(response.real_service_url, 0)
                    delay(100)
                    MPDCommandHandler.playSongIndex(0)
                    val clipboard =
                        context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clip: ClipData = ClipData.newPlainText(null, response.real_service_url)
                    clipboard.setPrimaryClip(clip)

                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_TEXT, response.real_service_url)
                        type = "text/plain"
                    }
                    val shareIntent = Intent.createChooser(sendIntent, null)
                    context.startActivity(shareIntent)

                } catch (t: Throwable) {
                    t.printStackTrace()
                }
            }
        }
        fm1Button.setOnClickListener(fmClick)
        fm1ButtonTop.setOnClickListener(fmClick)

        mHeaderView = findViewById(R.id.now_playing_headerLayout)
        mMainView = findViewById(R.id.now_playing_bodyLayout)

        // header buttons
        mTopPlayPauseButton = findViewById(R.id.now_playing_topPlayPauseButton)
        mTopPlaylistButton = findViewById(R.id.now_playing_topPlaylistButton)
        mTopMenuButton = findViewById(R.id.now_playing_topMenuButton)

        // bottom buttons
        mBottomRepeatButton = findViewById(R.id.now_playing_bottomRepeatButton)
        mBottomPreviousButton = findViewById(R.id.now_playing_bottomPreviousButton)
        mBottomPlayPauseButton = findViewById(R.id.now_playing_bottomPlayPauseButton)
        mBottomStopButton = findViewById(R.id.now_playing_bottomStopButton)
        mBottomNextButton = findViewById(R.id.now_playing_bottomNextButton)
        mBottomRandomButton = findViewById(R.id.now_playing_bottomRandomButton)

        // Main cover image
        mCoverImage = findViewById(R.id.now_playing_cover)
        // Small header cover image
        mTopCoverImage = findViewById(R.id.now_playing_topCover)

        // View with the ListView of the playlist
        mPlaylistView = findViewById(R.id.now_playing_playlist)

        // view switcher for cover and playlist view
        mViewSwitcher = findViewById(R.id.now_playing_view_switcher)

        // Button container for the buttons shown if dragged up
        mDraggedUpButtons = findViewById(R.id.now_playing_layout_dragged_up)
        // Button container for the buttons shown if dragged down
        mDraggedDownButtons = findViewById(R.id.now_playing_layout_dragged_down)

        // textviews
        mTrackName = findViewById(R.id.now_playing_trackName)
        // For marquee scrolling the TextView need selected == true
        mTrackName.setSelected(true)
        mTrackAdditionalInfo = findViewById(R.id.now_playing_track_additional_info)
        // For marquee scrolling the TextView need selected == true
        mTrackAdditionalInfo.setSelected(true)
        mTrackNo = findViewById(R.id.now_playing_text_track_no)
        mPlaylistNo = findViewById(R.id.now_playing_text_playlist_no)
        mBitrate = findViewById(R.id.now_playing_text_bitrate)
        mAudioProperties = findViewById(R.id.now_playing_text_audio_properties)
        mTrackURI = findViewById(R.id.now_playing_text_track_uri)

        // Textviews directly under the seekbar
        mElapsedTime = findViewById(R.id.now_playing_elapsedTime)
        mDuration = findViewById(R.id.now_playing_duration)
        mHeaderTextLayout = findViewById(R.id.now_playing_header_textLayout)

        // seekbar (position)
        mPositionSeekbar = findViewById(R.id.now_playing_seekBar)
        mPositionSeekbar.setOnSeekBarChangeListener(PositionSeekbarListener())
        mVolumeSeekbar = findViewById(R.id.volume_seekbar)
        mVolumeIcon = findViewById(R.id.volume_icon)
        mVolumeIcon.setOnClickListener(OnClickListener { view: View? ->
            MPDCommandHandler.setVolume(
                0
            )
        })
        mVolumeIcon.setOnLongClickListener(OnLongClickListener { view: View? ->
            MPDQueryHandler.getOutputs(OutputResponseMenuHandler(context, view))
            true
        })
        mVolumeSeekbar.setMax(100)
        mVolumeSeekbar.setOnSeekBarChangeListener(VolumeSeekBarListener())


        /* Volume control buttons */mVolumeIconButtons = findViewById(R.id.volume_icon_buttons)
        mVolumeIconButtons.setOnClickListener(OnClickListener { view: View? ->
            MPDCommandHandler.setVolume(
                0
            )
        })
        mVolumeIconButtons.setOnLongClickListener(OnLongClickListener { view: View? ->
            MPDQueryHandler.getOutputs(OutputResponseMenuHandler(context, view))
            true
        })
        mVolumeText = findViewById(R.id.volume_button_text)
        mVolumeMinus = findViewById(R.id.volume_button_minus)
        mVolumeMinus.setOnClickListener(OnClickListener { v: View? ->
            MPDCommandHandler.decreaseVolume(
                mVolumeStepSize
            )
        })
        mVolumePlus = findViewById(R.id.volume_button_plus)
        mVolumePlus.setOnClickListener(OnClickListener { v: View? ->
            MPDCommandHandler.increaseVolume(
                mVolumeStepSize
            )
        })

        /* Create two listeners that start a repeating timer task to repeat the volume plus/minus action */mPlusListener =
            VolumeButtonLongClickListener(
                VolumeButtonLongClickListener.LISTENER_ACTION.VOLUME_UP,
                mVolumeStepSize
            )
        mMinusListener = VolumeButtonLongClickListener(
            VolumeButtonLongClickListener.LISTENER_ACTION.VOLUME_DOWN,
            mVolumeStepSize
        )

        /* Set the listener to the plus/minus button */mVolumeMinus.setOnLongClickListener(
            mMinusListener
        )
        mVolumeMinus.setOnTouchListener(mMinusListener)
        mVolumePlus.setOnLongClickListener(mPlusListener)
        mVolumePlus.setOnTouchListener(mPlusListener)
        mVolumeSeekbarLayout = findViewById(R.id.volume_seekbar_layout)
        mVolumeButtonLayout = findViewById(R.id.volume_button_layout)

        // set dragging part default to bottom
        mDragOffset = 1.0f
        mDraggedUpButtons.setVisibility(INVISIBLE)
        mDraggedDownButtons.setVisibility(VISIBLE)
        mDraggedUpButtons.setAlpha(0.0f)

        // add listener to top playpause button
        mTopPlayPauseButton.setOnClickListener(OnClickListener { arg0: View? -> MPDCommandHandler.togglePause() })

        // Add listeners to top playlist button
        mTopPlaylistButton.setOnClickListener(OnClickListener { v: View? ->
            if (mViewSwitcher.getCurrentView() !== mPlaylistView) {
                setViewSwitcherStatus(VIEW_SWITCHER_STATUS.PLAYLIST_VIEW)
            } else {
                setViewSwitcherStatus(VIEW_SWITCHER_STATUS.COVER_VIEW)
            }

            // report the change of the view
            if (mDragStatusReceiver != null) {
                // set view status
                if (mViewSwitcher.getDisplayedChild() == 0) {
                    // cover image is shown
                    mDragStatusReceiver!!.onSwitchedViews(VIEW_SWITCHER_STATUS.COVER_VIEW)
                } else {
                    // playlist view is shown
                    mDragStatusReceiver!!.onSwitchedViews(VIEW_SWITCHER_STATUS.PLAYLIST_VIEW)
                    mPlaylistView.jumpToCurrentSong()
                }
            }
        })
        TooltipCompat.setTooltipText(
            mTopPlaylistButton,
            resources.getString(R.string.action_npv_show_playlist)
        )

        // Add listener to top menu button
        mTopMenuButton.setOnClickListener(OnClickListener { v: View -> showAdditionalOptionsMenu(v) })
        TooltipCompat.setTooltipText(
            mTopMenuButton,
            resources.getString(R.string.action_npv_more_options)
        )

        // Add listener to bottom repeat button
        mBottomRepeatButton.setOnClickListener(OnClickListener { arg0: View? ->
            if (null != mLastStatus) {
                if (mLastStatus!!.repeat == 0) {
                    MPDCommandHandler.setRepeat(true)
                } else {
                    MPDCommandHandler.setRepeat(false)
                }
            }
        })

        // Add listener to bottom previous button
        mBottomPreviousButton.setOnClickListener(OnClickListener { arg0: View? -> MPDCommandHandler.previousSong() })

        // Add listener to bottom playpause button
        mBottomPlayPauseButton.setOnClickListener(OnClickListener { arg0: View? -> MPDCommandHandler.togglePause() })
        mBottomStopButton.setOnClickListener(OnClickListener { view: View? -> MPDCommandHandler.stop() })

        // Add listener to bottom next button
        mBottomNextButton.setOnClickListener(OnClickListener { arg0: View? -> MPDCommandHandler.nextSong() })

        // Add listener to bottom random button
        mBottomRandomButton.setOnClickListener(OnClickListener { arg0: View? ->
            if (null != mLastStatus) {
                if (mLastStatus!!.random == 0) {
                    MPDCommandHandler.setRandom(true)
                } else {
                    MPDCommandHandler.setRandom(false)
                }
            }
        })
        mCoverImage.setOnClickListener(OnClickListener { v: View? ->
            val intent = Intent(context, FanartActivity::class.java)
            context.startActivity(intent)
        })
        mCoverImage.setVisibility(INVISIBLE)
        mCoverLoader = CoverBitmapLoader(context, CoverReceiverClass())
    }

    /**
     * Called to open the popup menu on the top right corner.
     *
     * @param v
     */
    private fun showAdditionalOptionsMenu(v: View) {
        val popupMenu = PopupMenu(context, v)
        // Inflate the menu from a menu xml file
        popupMenu.inflate(R.menu.popup_menu_nowplaying)
        // Set the main NowPlayingView as a listener (directly implements callback)
        popupMenu.setOnMenuItemClickListener(this)
        // Real menu
        val menu = popupMenu.menu

        // Set the checked menu item state if a MPDCurrentStatus is available
        if (null != mLastStatus) {
            val singlePlaybackItem = menu.findItem(R.id.action_toggle_single_mode)
            singlePlaybackItem.isChecked = mLastStatus!!.singlePlayback == 1
            val consumeItem = menu.findItem(R.id.action_toggle_consume_mode)
            consumeItem.isChecked = mLastStatus!!.consume == 1
        }

        // Check if the current view is the cover or the playlist. If it is the playlist hide its actions.
        // If the viewswitcher only has one child the dual pane layout is used
        if (mViewSwitcher!!.displayedChild == 0 && mViewSwitcher!!.childCount > 1) {
            menu.setGroupEnabled(R.id.group_playlist_actions, false)
            menu.setGroupVisible(R.id.group_playlist_actions, false)
        }

        // Check if streaming is configured for the current server
        val streamingEnabled =
            ConnectionManager.getInstance(context.applicationContext).streamingEnabled
        val streamingStartStopItem = menu.findItem(R.id.action_start_streaming)
        if (!streamingEnabled) {
            streamingStartStopItem.isVisible = false
        } else {
            if (mStreamingStatus == STREAMING_STATUS.PLAYING || mStreamingStatus == STREAMING_STATUS.BUFFERING) {
                streamingStartStopItem.title = resources.getString(R.string.action_stop_streaming)
            } else {
                streamingStartStopItem.title = resources.getString(R.string.action_start_streaming)
            }
        }

        // Open the menu itself
        popupMenu.show()
    }

    /**
     * Called when a layout is requested from the graphics system.
     *
     * @param changed If the layout is changed (size, ...)
     * @param l       Left position
     * @param t       Top position
     * @param r       Right position
     * @param b       Bottom position
     */
    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        // Calculate the maximal range that the view is allowed to be dragged
        mDragRange = measuredHeight - mHeaderView!!.measuredHeight

        // New temporary top position, to fix the view at top or bottom later if state is idle.
        var newTop = mTopPosition

        // fix height at top or bottom if state idle
        if (mDragHelper.viewDragState == ViewDragHelper.STATE_IDLE) {
            newTop = (mDragRange * mDragOffset).toInt()
        }

        // Request the upper part of the NowPlayingView (header)
        mHeaderView!!.layout(
            0,
            newTop,
            r,
            newTop + mHeaderView!!.measuredHeight
        )

        // Request the lower part of the NowPlayingView (main part)
        mMainView!!.layout(
            0,
            newTop + mHeaderView!!.measuredHeight,
            r,
            newTop + b
        )
    }

    /**
     * Stop the refresh timer when the view is not visible to the user anymore.
     * Unregister the receiver for NowPlayingInformation intends, not needed anylonger.
     */
    fun onPause() {
        // Unregister listener
        MPDStateMonitoringHandler.getHandler().unregisterStatusListener(mStateListener)
        MPDInterface.getGenericInstance()
            .removeMPDConnectionStateChangeListener(mConnectionStateListener)
        mPlaylistView!!.onPause()
        if (null != mBackgroundServiceConnection) {
            mBackgroundServiceConnection!!.closeConnection()
            mBackgroundServiceConnection = null
        }
        context.applicationContext.unregisterReceiver(mStreamingStatusReceiver)
        ArtworkManager.getInstance(context.applicationContext)
            .unregisterOnNewAlbumImageListener(this)
        ArtworkManager.getInstance(context.applicationContext)
            .unregisterOnNewArtistImageListener(this)
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        sharedPref.unregisterOnSharedPreferenceChangeListener(this)
    }

    /**
     * Resumes refreshing operation because the view is visible to the user again.
     * Also registers to the NowPlayingInformation intends again.
     */
    fun onResume() {

        // get the playbackservice, when the connection is successfully established the timer gets restarted

        // Reenable scrolling views after resuming
        if (mTrackName != null) {
            mTrackName!!.isSelected = true
        }
        if (mTrackAdditionalInfo != null) {
            mTrackAdditionalInfo!!.isSelected = true
        }
        if (mStreamingStatusReceiver == null) {
            mStreamingStatusReceiver = StreamingStatusReceiver()
        }
        if (null == mBackgroundServiceConnection) {
            mBackgroundServiceConnection = BackgroundServiceConnection(
                context.applicationContext,
                BackgroundServiceConnectionListener()
            )
        }
        mBackgroundServiceConnection!!.openConnection()
        val filter = IntentFilter()
        filter.addAction(BackgroundService.ACTION_STREAMING_STATUS_CHANGED)
        context.applicationContext.registerReceiver(mStreamingStatusReceiver, filter)

        // Register with MPDStateMonitoring system
        MPDStateMonitoringHandler.getHandler().registerStatusListener(mStateListener)
        MPDInterface.getGenericInstance()
            .addMPDConnectionStateChangeListener(mConnectionStateListener)
        mPlaylistView!!.onResume()
        ArtworkManager.getInstance(context.applicationContext).registerOnNewAlbumImageListener(this)
        ArtworkManager.getInstance(context.applicationContext)
            .registerOnNewArtistImageListener(this)
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        mShowNPVHint =
            sharedPref.getBoolean(context.resources.getString(R.string.pref_show_npv_hint), true)
        sharedPref.registerOnSharedPreferenceChangeListener(this)
        setVolumeControlSetting()
        mUseEnglishWikipedia = sharedPref.getBoolean(
            context.getString(R.string.pref_use_english_wikipedia_key),
            context.resources.getBoolean(R.bool.pref_use_english_wikipedia_default)
        )
        mShowArtistImage = sharedPref.getBoolean(
            context.getString(R.string.pref_show_npv_artist_image_key),
            context.resources.getBoolean(R.bool.pref_show_npv_artist_image_default)
        )
    }

    private fun updateMPDStatus(status: MPDCurrentStatus) {
        val state = status.playbackState
        when (state) {
            MPD_PLAYBACK_STATE.MPD_PLAYING -> {
                mTopPlayPauseButton!!.setImageResource(R.drawable.ic_pause_48dp)
                mBottomPlayPauseButton!!.setImageResource(R.drawable.ic_pause_circle_fill_48dp)

                // show the hint if necessary
                if (mShowNPVHint) {
                    showHint()
                }
            }
            MPD_PLAYBACK_STATE.MPD_PAUSING, MPD_PLAYBACK_STATE.MPD_STOPPED -> {
                mTopPlayPauseButton!!.setImageResource(R.drawable.ic_play_arrow_48dp)
                mBottomPlayPauseButton!!.setImageResource(R.drawable.ic_play_circle_fill_48dp)
            }
        }
        when (status.repeat) {
            0 -> {
                mBottomRepeatButton!!.setImageResource(R.drawable.ic_repeat_24dp)
                mBottomRepeatButton!!.imageTintList = ColorStateList.valueOf(
                    ThemeUtils.getThemeColor(
                        context,
                        R.attr.malp_color_text_accent
                    )
                )
            }
            1 -> {
                mBottomRepeatButton!!.setImageResource(R.drawable.ic_repeat_24dp)
                mBottomRepeatButton!!.imageTintList = ColorStateList.valueOf(
                    ThemeUtils.getThemeColor(
                        context,
                        android.R.attr.colorAccent
                    )
                )
            }
        }
        when (status.random) {
            0 -> mBottomRandomButton!!.imageTintList = ColorStateList.valueOf(
                ThemeUtils.getThemeColor(
                    context,
                    R.attr.malp_color_text_accent
                )
            )
            1 -> mBottomRandomButton!!.imageTintList = ColorStateList.valueOf(
                ThemeUtils.getThemeColor(
                    context,
                    android.R.attr.colorAccent
                )
            )
        }
        val elapsed = Math.round(status.elapsedTime)
        val length = Math.round(status.trackLength)
        // Update position seekbar & textviews
        mPositionSeekbar!!.max = length
        if (!mPositionSeekbar!!.isPressed) {
            mPositionSeekbar!!.progress = elapsed
        }
        mElapsedTime!!.text = FormatHelper.formatTracktimeFromS(elapsed.toLong())
        mDuration!!.text = FormatHelper.formatTracktimeFromS(length.toLong())

        // Update volume seekbar
        val volume = status.volume
        if (!mVolumeSeekbar!!.isPressed) {
            mVolumeSeekbar!!.progress = volume
        }
        if (volume >= 70) {
            mVolumeIcon!!.setImageResource(R.drawable.ic_volume_high_black_48dp)
            mVolumeIconButtons!!.setImageResource(R.drawable.ic_volume_high_black_48dp)
        } else if (volume >= 30) {
            mVolumeIcon!!.setImageResource(R.drawable.ic_volume_medium_black_48dp)
            mVolumeIconButtons!!.setImageResource(R.drawable.ic_volume_medium_black_48dp)
        } else if (volume > 0) {
            mVolumeIcon!!.setImageResource(R.drawable.ic_volume_low_black_48dp)
            mVolumeIconButtons!!.setImageResource(R.drawable.ic_volume_low_black_48dp)
        } else {
            mVolumeIcon!!.setImageResource(R.drawable.ic_volume_mute_black_48dp)
            mVolumeIconButtons!!.setImageResource(R.drawable.ic_volume_mute_black_48dp)
        }
        mVolumeIcon!!.imageTintList =
            ColorStateList.valueOf(ThemeUtils.getThemeColor(context, R.attr.malp_color_text_accent))
        mVolumeIconButtons!!.imageTintList =
            ColorStateList.valueOf(ThemeUtils.getThemeColor(context, R.attr.malp_color_text_accent))
        mVolumeText!!.text = resources.getString(R.string.volume_level_template, volume)
        mPlaylistNo!!.text = resources.getString(
            R.string.track_number_template,
            status.currentSongIndex + 1,
            status.playlistLength
        )
        mLastStatus = status
        mBitrate!!.text =
            resources.getString(R.string.bitrate_unit_kilo_bits_template, status.bitrate)

        // Set audio properties string
        val propertiesBuilder = StringBuilder()
        propertiesBuilder.append(
            resources.getString(
                R.string.samplerate_unit_hertz_template,
                status.samplerate
            )
        )
        propertiesBuilder.append(' ')

        // Check for fancy new formats here (dsd, float = f)
        val sampleFormat = status.bitDepth
        when (sampleFormat) {
            "16", "24", "8", "32" -> propertiesBuilder.append(
                resources.getString(
                    R.string.sampleformat_unit_bits_template,
                    sampleFormat
                )
            )
            "f" -> propertiesBuilder.append("float")
            else -> propertiesBuilder.append(sampleFormat)
        }
        propertiesBuilder.append(' ')
        propertiesBuilder.append(
            resources.getString(
                R.string.channels_template,
                status.channelCount
            )
        )
        mAudioProperties!!.text = propertiesBuilder.toString()
    }

    private fun updateMPDCurrentTrack(track: MPDTrack) {
        // Check if track title is set, otherwise use track name, otherwise path
        val title = track.visibleTitle
        mTrackName!!.text = title
        mTrackAdditionalInfo!!.text = track.getSubLine(context)
        if (null == mLastTrack || !track.equalsStringTag(
                MPDTrack.StringTagTypes.ALBUM,
                mLastTrack
            ) || !track.equalsStringTag(MPDTrack.StringTagTypes.ALBUM_MBID, mLastTrack)
        ) {
            // Show the placeholder image until the cover fetch process finishes
            mCoverImage!!.clearAlbumImage()

            // The same for the small header image
            val tintColor = ThemeUtils.getThemeColor(context, R.attr.malp_color_text_accent)
            var drawable =
                ResourcesCompat.getDrawable(resources, R.drawable.cover_placeholder_128dp, null)
            if (drawable != null) {
                drawable = DrawableCompat.wrap(drawable)
                DrawableCompat.setTint(drawable, tintColor)
            }
            mTopCoverImage!!.setImageDrawable(drawable)
            // Start the cover loader
            mCoverLoader!!.getImage(track, true, mCoverImage!!.width, mCoverImage!!.height)
        }
        if (mShowArtistImage && (null == mLastTrack || !track.equalsStringTag(
                MPDTrack.StringTagTypes.ARTIST,
                mLastTrack
            ) || !track.equalsStringTag(MPDTrack.StringTagTypes.ARTIST_MBID, mLastTrack))
        ) {
            mCoverImage!!.clearArtistImage()
            mCoverLoader!!.getArtistImage(track, true, mCoverImage!!.width, mCoverImage!!.height)
        }

        // Calculate the margin to avoid cut off textviews
        val layoutParams = mHeaderTextLayout!!.layoutParams as LayoutParams
        layoutParams.marginEnd = (mTopPlaylistButton!!.width * (1.0 - mDragOffset)).toInt()
        mHeaderTextLayout!!.layoutParams = layoutParams
        mTrackURI!!.text = track.path
        if (track.albumTrackCount != 0) {
            mTrackNo!!.text = resources.getString(
                R.string.track_number_template,
                track.trackNumber,
                track.albumTrackCount
            )
        } else {
            mTrackNo!!.text = track.trackNumber.toString()
        }
        mLastTrack = track
    }

    /**
     * Can be used to register an observer to this view, that is notified when a change of the dragstatus,offset happens.
     *
     * @param receiver Observer to register, only one observer at a time is possible.
     */
    fun registerDragStatusReceiver(receiver: NowPlayingDragStatusReceiver?) {
        mDragStatusReceiver = receiver
        // Initial status notification
        if (mDragStatusReceiver != null) {

            // set drag status
            if (mDragOffset == 0.0f) {
                // top
                mDragStatusReceiver!!.onStatusChanged(DRAG_STATUS.DRAGGED_UP)
            } else {
                // bottom
                mDragStatusReceiver!!.onStatusChanged(DRAG_STATUS.DRAGGED_DOWN)
            }

            // set view status
            if (mViewSwitcher!!.displayedChild == 0) {
                // cover image is shown
                mDragStatusReceiver!!.onSwitchedViews(VIEW_SWITCHER_STATUS.COVER_VIEW)
            } else {
                // playlist view is shown
                mDragStatusReceiver!!.onSwitchedViews(VIEW_SWITCHER_STATUS.PLAYLIST_VIEW)
            }
        }
    }

    /**
     * Set the viewswitcher of cover/playlist view to the requested state.
     *
     * @param view the view which should be displayed.
     */
    fun setViewSwitcherStatus(view: VIEW_SWITCHER_STATUS?) {
        var color = 0
        when (view) {
            VIEW_SWITCHER_STATUS.COVER_VIEW -> {
                // change the view only if the requested view is not displayed
                if (mViewSwitcher!!.currentView !== mCoverImage) {
                    mViewSwitcher!!.showNext()
                }
                color = ThemeUtils.getThemeColor(context, android.R.attr.textColor)
                TooltipCompat.setTooltipText(
                    mTopPlaylistButton!!,
                    resources.getString(R.string.action_npv_show_playlist)
                )
            }
            VIEW_SWITCHER_STATUS.PLAYLIST_VIEW -> {
                // change the view only if the requested view is not displayed
                if (mViewSwitcher!!.currentView !== mPlaylistView) {
                    mViewSwitcher!!.showNext()
                }
                color = ThemeUtils.getThemeColor(context, R.attr.colorAccent)
                TooltipCompat.setTooltipText(
                    mTopPlaylistButton!!,
                    resources.getString(R.string.action_npv_show_cover)
                )
            }
        }

        // tint the button according to the requested view
        mTopPlaylistButton!!.imageTintList = ColorStateList.valueOf(color)
    }

    /**
     * This method will drag up the NPV a little for 1 second.
     *
     *
     * This should be called only the first time music is played with Odyssey.
     */
    private fun showHint() {
        mShowNPVHint = false
        val sharedPrefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit()
        sharedPrefEditor.putBoolean(context.getString(R.string.pref_show_npv_hint), false)
        sharedPrefEditor.apply()
        if (mDragOffset == 1.0f) {
            // show hint only if the npv ist not already dragged up
            smoothSlideTo(0.75f)
            val handler = Handler()
            handler.postDelayed({
                if (mDragOffset > 0.0f) {
                    smoothSlideTo(1.0f)
                }
            }, 3000)
        }
    }

    /**
     * Simple sharing for the current track.
     *
     *
     * This will only work if the track can be found in the mediastore.
     */
    private fun shareCurrentTrack() {
        if (null == mLastTrack) {
            return
        }
        val sharingText = context.getString(
            R.string.sharing_song_details,
            mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.TITLE),
            mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ARTIST),
            mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ALBUM)
        )

        // set up intent for sharing
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, sharingText)
        shareIntent.type = "text/plain"

        // start sharing
        context.startActivity(
            Intent.createChooser(
                shareIntent,
                context.getString(R.string.dialog_share_song_details)
            )
        )
    }

    /**
     * Public interface used by observers to be notified about a change in drag state or drag position.
     */
    interface NowPlayingDragStatusReceiver {
        // Possible values for DRAG_STATUS (up,down)
        enum class DRAG_STATUS {
            DRAGGED_UP, DRAGGED_DOWN
        }

        // Possible values for the view in the viewswitcher (cover, playlist)
        enum class VIEW_SWITCHER_STATUS {
            COVER_VIEW, PLAYLIST_VIEW
        }

        // Called when the whole view is either completely dragged up or down
        fun onStatusChanged(status: DRAG_STATUS)

        // Called continuously during dragging.
        fun onDragPositionChanged(pos: Float)

        // Called when the view switcher switches between cover and playlist view
        fun onSwitchedViews(view: VIEW_SWITCHER_STATUS)

        // Called when the user starts the drag
        fun onStartDrag()
    }

    private class ServerStatusListener internal constructor(nowPlayingView: NowPlayingView) :
        MPDStatusChangeHandler() {
        private val mNowPlayingView: WeakReference<NowPlayingView>
        override fun onNewStatusReady(status: MPDCurrentStatus) {
            val nowPlayingView = mNowPlayingView.get()
            nowPlayingView?.updateMPDStatus(status)
        }

        override fun onNewTrackReady(track: MPDTrack) {
            val nowPlayingView = mNowPlayingView.get()
            nowPlayingView?.updateMPDCurrentTrack(track)
        }

        init {
            mNowPlayingView = WeakReference(nowPlayingView)
        }
    }

    private class ServerConnectionListener internal constructor(
        npv: NowPlayingView,
        looper: Looper?
    ) : MPDConnectionStateChangeHandler(looper) {
        private val mNPV: WeakReference<NowPlayingView>
        override fun onConnected() {
            mNPV.get()!!.updateMPDStatus(MPDStateMonitoringHandler.getHandler().lastStatus)
        }

        override fun onDisconnected() {
            mNPV.get()!!.updateMPDStatus(MPDCurrentStatus())
            mNPV.get()!!.updateMPDCurrentTrack(MPDTrack(""))
        }

        init {
            mNPV = WeakReference(npv)
        }
    }

    private class PositionSeekbarListener : OnSeekBarChangeListener {
        /**
         * Called if the user drags the seekbar to a new position or the seekbar is altered from
         * outside. Just do some seeking, if the action is done by the user.
         *
         * @param seekBar  Seekbar of which the progress was changed.
         * @param progress The new position of the seekbar.
         * @param fromUser If the action was initiated by the user.
         */
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                // FIXME Check if it is better to just update if user releases the seekbar
                // (network stress)
                MPDCommandHandler.seekSeconds(progress)
            }
        }

        /**
         * Called if the user starts moving the seekbar. We do not handle this for now.
         *
         * @param seekBar SeekBar that is used for dragging.
         */
        override fun onStartTrackingTouch(seekBar: SeekBar) {
            // TODO Auto-generated method stub
        }

        /**
         * Called if the user ends moving the seekbar. We do not handle this for now.
         *
         * @param seekBar SeekBar that is used for dragging.
         */
        override fun onStopTrackingTouch(seekBar: SeekBar) {
            // TODO Auto-generated method stub
        }
    }

    private inner class VolumeSeekBarListener : OnSeekBarChangeListener {
        /**
         * Called if the user drags the seekbar to a new position or the seekbar is altered from
         * outside. Just do some seeking, if the action is done by the user.
         *
         * @param seekBar  Seekbar of which the progress was changed.
         * @param progress The new position of the seekbar.
         * @param fromUser If the action was initiated by the user.
         */
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                MPDCommandHandler.setVolume(progress)
                if (progress >= 70) {
                    mVolumeIcon!!.setImageResource(R.drawable.ic_volume_high_black_48dp)
                } else if (progress >= 30) {
                    mVolumeIcon!!.setImageResource(R.drawable.ic_volume_medium_black_48dp)
                } else if (progress > 0) {
                    mVolumeIcon!!.setImageResource(R.drawable.ic_volume_low_black_48dp)
                } else {
                    mVolumeIcon!!.setImageResource(R.drawable.ic_volume_mute_black_48dp)
                }
            }
        }

        /**
         * Called if the user starts moving the seekbar. We do not handle this for now.
         *
         * @param seekBar SeekBar that is used for dragging.
         */
        override fun onStartTrackingTouch(seekBar: SeekBar) {
            // TODO Auto-generated method stub
        }

        /**
         * Called if the user ends moving the seekbar. We do not handle this for now.
         *
         * @param seekBar SeekBar that is used for dragging.
         */
        override fun onStopTrackingTouch(seekBar: SeekBar) {
            // TODO Auto-generated method stub
        }
    }

    /**
     * Private class that handles when the CoverGenerator finishes its fetching of cover images.
     */
    private inner class CoverReceiverClass : CoverBitmapListener {
        /**
         * Called when a bitmap is created
         *
         * @param bm Bitmap ready for use in the UI
         */
        override fun receiveBitmap(bm: Bitmap?, type: IMAGE_TYPE?) {
            if (bm != null) {
                val activity = context as? Activity
                activity?.runOnUiThread {
                    if (type == IMAGE_TYPE.ALBUM_IMAGE) {
                        // Set the main cover image
                        mCoverImage.setAlbumImage(bm)
                        // Set the small header image
                        mTopCoverImage!!.setImageBitmap(bm)
                    } else if (type == IMAGE_TYPE.ARTIST_IMAGE) {
                        mCoverImage.setArtistImage(bm)
                    }
                }
            }
        }
    }

    /**
     * Receives stream playback status updates. When stream playback is started the status
     * is necessary to show the right menu item.
     */
    private inner class StreamingStatusReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (BackgroundService.ACTION_STREAMING_STATUS_CHANGED == intent.action) {
                mStreamingStatus = STREAMING_STATUS.values()[intent.getIntExtra(
                    BackgroundService.INTENT_EXTRA_STREAMING_STATUS,
                    0
                )]
            }
        }
    }

    /**
     * Private class to handle when a [android.content.ServiceConnection] to the [BackgroundService]
     * is established. When the connection is established, the stream playback status is retrieved.
     */
    private inner class BackgroundServiceConnectionListener : OnConnectionStatusChangedListener {
        override fun onConnected() {
            try {
                mStreamingStatus =
                    STREAMING_STATUS.values()[mBackgroundServiceConnection!!.service.streamingStatus]
            } catch (ignored: RemoteException) {
            }
        }

        override fun onDisconnected() {}
    }

    companion object {
        private val TAG = NowPlayingView::class.java.simpleName
    }

    init {
        mDragHelper = ViewDragHelper.create(this, 1f, BottomDragCallbackHelper())
        mStateListener = ServerStatusListener(this)
        mConnectionStateListener = ServerConnectionListener(this, getContext().mainLooper)
        mLastStatus = MPDCurrentStatus()
        mLastTrack = MPDTrack("")
    }
}