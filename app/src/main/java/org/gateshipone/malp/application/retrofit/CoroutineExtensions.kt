package org.gateshipone.malp.application.retrofit

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.withTimeout

// Coroutine Default Exception Handler
val coroutineExceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->

}

/**
 * retry block
 */
suspend fun <T> retryIO(
        times: Int = Int.MAX_VALUE,
        initialDelay: Long = 0, // 0.1 second
        maxDelay: Long = 1000,    // 1 second
        factor: Double = 2.0,
        timeout: Long = 5000,
        block: suspend () -> T,
): T {
    var currentDelay = initialDelay
    repeat(times - 1) {
        try {
            return withTimeout(timeout) { block() }
        } catch (e: Throwable) {
        }
        delay(currentDelay)
        currentDelay = (currentDelay * factor).toLong().coerceAtMost(maxDelay)
    }
    return withTimeout(timeout) { block() } // last attempt
}

@ExperimentalCoroutinesApi
fun <T> StateFlow<T>.push(value: T) {
    if (this is MutableStateFlow<T>) {
        this.value = value
    }
}

@ExperimentalCoroutinesApi
fun <T> SharedFlow<T>.push(value: T) {
    if (this is MutableSharedFlow<T>) {
        this.tryEmit(value)
    }
}