package org.gateshipone.malp.application.retrofit

import com.facebook.flipper.plugins.network.FlipperOkhttpInterceptor
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Retro {
    val kbsService by lazy { adapter.create(KbsService::class.java) }

    private val client: OkHttpClient = createOkHttpClient()
    private val adapter: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("https://onair.kbs.co.kr")
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .client(client)
            .build()
    }

    private fun createOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(20, TimeUnit.SECONDS)
        builder.readTimeout(20, TimeUnit.SECONDS)
        builder.writeTimeout(20, TimeUnit.SECONDS)

        builder.addInterceptor { chain ->
            chain.proceed(chain.request().newBuilder().build())
        }
        builder.addNetworkInterceptor(FlipperOkhttpInterceptor(FlipperHelper.networkFlipperPlugin))
        return builder.build()
    }
}