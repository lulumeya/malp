/*
 *  Copyright (C) 2020 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/gateship-one/malp/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.gateshipone.malp.application.activities

import org.gateshipone.malp.application.activities.GenericActivity
import org.gateshipone.malp.application.artwork.FanartManager.OnFanartCacheChangeListener
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDTrack
import org.gateshipone.malp.application.utils.VolumeButtonLongClickListener
import org.gateshipone.malp.application.artwork.FanartManager
import android.os.Bundle
import org.gateshipone.malp.R
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDCommandHandler
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDStateMonitoringHandler
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDCurrentStatus
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDException.MPDServerException
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDException.MPDConnectionException
import org.gateshipone.malp.application.activities.FanartActivity
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDCurrentStatus.MPD_PLAYBACK_STATE
import android.content.res.ColorStateList
import org.gateshipone.malp.application.artwork.network.MALPRequestQueue
import com.android.volley.RequestQueue.RequestFilter
import org.gateshipone.malp.application.artwork.network.requests.FanartImageRequest
import org.gateshipone.malp.mpdservice.handlers.MPDStatusChangeHandler
import android.graphics.Bitmap
import org.gateshipone.malp.application.activities.FanartActivity.ViewSwitchTask
import android.widget.SeekBar.OnSeekBarChangeListener
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.View
import android.widget.*
import com.android.volley.Request
import org.gateshipone.malp.application.utils.ThemeUtils
import java.lang.ref.WeakReference
import java.util.*

class FanartActivity : GenericActivity(), OnFanartCacheChangeListener {
    private var mTrackTitle: TextView? = null
    private var mTrackAlbum: TextView? = null
    private var mTrackArtist: TextView? = null
    private var mLastTrack: MPDTrack? = null
    private var mStateListener: ServerStatusListener? = null
    private lateinit var mSwitcher: ViewSwitcher
    private var mSwitchTimer: Timer? = null
    private var mNextFanart = 0
    private var mCurrentFanart = 0
    private var mFanartView0: ImageView? = null
    private var mFanartView1: ImageView? = null
    private lateinit var mPlayPauseButton: ImageButton

    /**
     * Seekbar used for seeking and informing the user of the current playback position.
     */
    private lateinit var mPositionSeekbar: SeekBar

    /**
     * Seekbar used for volume control of host
     */
    private lateinit var mVolumeSeekbar: SeekBar
    private lateinit var mVolumeIcon: ImageView
    private lateinit var mVolumeIconButtons: ImageView
    private var mVolumeText: TextView? = null
    private var mPlusListener: VolumeButtonLongClickListener? = null
    private var mMinusListener: VolumeButtonLongClickListener? = null
    private var mVolumeSeekbarLayout: LinearLayout? = null
    private var mVolumeButtonLayout: LinearLayout? = null
    private var mVolumeStepSize = 1
    private var mFanartManager: FanartManager? = null
    var mDecorView: View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDecorView = window.decorView
        setContentView(R.layout.activity_artist_fanart)
        mTrackTitle = findViewById(R.id.textview_track_title)
        mTrackAlbum = findViewById(R.id.textview_track_album)
        mTrackArtist = findViewById(R.id.textview_track_artist)
        mSwitcher = findViewById(R.id.fanart_switcher)
        mFanartView0 = findViewById(R.id.fanart_view_0)
        mFanartView1 = findViewById(R.id.fanart_view_1)
        val previousButton = findViewById<ImageButton>(R.id.button_previous_track)
        previousButton.setOnClickListener { v: View? -> MPDCommandHandler.previousSong() }
        val nextButton = findViewById<ImageButton>(R.id.button_next_track)
        nextButton.setOnClickListener { v: View? -> MPDCommandHandler.nextSong() }
        val stopButton = findViewById<ImageButton>(R.id.button_stop)
        stopButton.setOnClickListener { view: View? -> MPDCommandHandler.stop() }
        mPlayPauseButton = findViewById(R.id.button_playpause)
        mPlayPauseButton.setOnClickListener(View.OnClickListener { view: View? -> MPDCommandHandler.togglePause() })
        if (null == mStateListener) {
            mStateListener = ServerStatusListener(this)
        }
        mSwitcher.setOnClickListener(View.OnClickListener { v: View? ->
            cancelSwitching()
            startSwitching()
            updateFanartViews()
        })

        // seekbar (position)
        mPositionSeekbar = findViewById(R.id.now_playing_seekBar)
        mPositionSeekbar.setOnSeekBarChangeListener(PositionSeekbarListener())
        mVolumeSeekbar = findViewById(R.id.volume_seekbar)
        mVolumeIcon = findViewById(R.id.volume_icon)
        mVolumeIcon.setOnClickListener(View.OnClickListener { view: View? -> MPDCommandHandler.setVolume(0) })
        mVolumeSeekbar.setMax(100)
        mVolumeSeekbar.setOnSeekBarChangeListener(VolumeSeekBarListener())

        /* Volume control buttons */mVolumeIconButtons = findViewById(R.id.volume_icon_buttons)
        mVolumeIconButtons.setOnClickListener(View.OnClickListener { view: View? -> MPDCommandHandler.setVolume(0) })
        mVolumeText = findViewById(R.id.volume_button_text)

        /* Create two listeners that start a repeating timer task to repeat the volume plus/minus action */mPlusListener = VolumeButtonLongClickListener(VolumeButtonLongClickListener.LISTENER_ACTION.VOLUME_UP, mVolumeStepSize)
        mMinusListener = VolumeButtonLongClickListener(VolumeButtonLongClickListener.LISTENER_ACTION.VOLUME_DOWN, mVolumeStepSize)
        val volumeMinus = findViewById<ImageButton>(R.id.volume_button_minus)
        volumeMinus.setOnClickListener { v: View? -> MPDCommandHandler.decreaseVolume(mVolumeStepSize) }
        volumeMinus.setOnLongClickListener(mMinusListener)
        volumeMinus.setOnTouchListener(mPlusListener)
        val volumePlus = findViewById<ImageButton>(R.id.volume_button_plus)
        volumePlus.setOnClickListener { v: View? -> MPDCommandHandler.increaseVolume(mVolumeStepSize) }
        volumePlus.setOnLongClickListener(mPlusListener)
        volumePlus.setOnTouchListener(mPlusListener)
        mVolumeSeekbarLayout = findViewById(R.id.volume_seekbar_layout)
        mVolumeButtonLayout = findViewById(R.id.volume_button_layout)
        mFanartManager = FanartManager.getInstance(applicationContext)
    }

    override fun onResume() {
        super.onResume()
        MPDStateMonitoringHandler.getHandler().registerStatusListener(mStateListener)
        cancelSwitching()
        startSwitching()
        mTrackTitle!!.isSelected = true
        mTrackArtist!!.isSelected = true
        mTrackAlbum!!.isSelected = true
        hideSystemUI()
        setVolumeControlSetting()
    }

    override fun onPause() {
        super.onPause()
        MPDStateMonitoringHandler.getHandler().unregisterStatusListener(mStateListener)
        cancelSwitching()
    }

    override fun onConnected() {
        updateMPDStatus(MPDStateMonitoringHandler.getHandler().lastStatus)
    }

    override fun onDisconnected() {
        updateMPDStatus(MPDCurrentStatus())
        updateMPDCurrentTrack(MPDTrack(""))
    }

    override fun onMPDError(e: MPDServerException?) {}
    override fun onMPDConnectionError(e: MPDConnectionException?) {}
    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putInt(STATE_ARTWORK_POINTER, mCurrentFanart)
        savedInstanceState.putInt(STATE_ARTWORK_POINTER_NEXT, mNextFanart)
        savedInstanceState.putParcelable(STATE_LAST_TRACK, mLastTrack)

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState)
    }

    public override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState)

        // Restore state members from saved instance
        mCurrentFanart = savedInstanceState.getInt(STATE_ARTWORK_POINTER)
        mNextFanart = savedInstanceState.getInt(STATE_ARTWORK_POINTER_NEXT)
        mLastTrack = savedInstanceState.getParcelable(STATE_LAST_TRACK)
        restoreFanartView()
    }

    override fun fanartInitialCacheCount(count: Int) {
        if (count > 0) {
            mNextFanart = 0
            updateFanartViews()
        }
    }

    override fun fanartCacheCountChanged(count: Int) {
        if (count == 1) {
            updateFanartViews()
        }
        if (mCurrentFanart == count - 2) {
            mNextFanart = (mCurrentFanart + 1) % count
        }
    }

    /**
     * Updates the system control with a new MPD status.
     *
     * @param status The current MPD status.
     */
    private fun updateMPDStatus(status: MPDCurrentStatus) {
        val state = status.playbackState
        when (state) {
            MPD_PLAYBACK_STATE.MPD_PLAYING -> mPlayPauseButton!!.setImageResource(R.drawable.ic_pause_circle_fill_48dp)
            MPD_PLAYBACK_STATE.MPD_PAUSING, MPD_PLAYBACK_STATE.MPD_STOPPED -> mPlayPauseButton!!.setImageResource(R.drawable.ic_play_circle_fill_48dp)
        }

        // Update volume seekbar
        val volume = status.volume
        mVolumeSeekbar!!.progress = volume
        if (volume >= 70) {
            mVolumeIcon!!.setImageResource(R.drawable.ic_volume_high_black_48dp)
            mVolumeIconButtons!!.setImageResource(R.drawable.ic_volume_high_black_48dp)
        } else if (volume >= 30) {
            mVolumeIcon!!.setImageResource(R.drawable.ic_volume_medium_black_48dp)
            mVolumeIconButtons!!.setImageResource(R.drawable.ic_volume_medium_black_48dp)
        } else if (volume > 0) {
            mVolumeIcon!!.setImageResource(R.drawable.ic_volume_low_black_48dp)
            mVolumeIconButtons!!.setImageResource(R.drawable.ic_volume_low_black_48dp)
        } else {
            mVolumeIcon!!.setImageResource(R.drawable.ic_volume_mute_black_48dp)
            mVolumeIconButtons!!.setImageResource(R.drawable.ic_volume_mute_black_48dp)
        }
        mVolumeIcon!!.imageTintList = ColorStateList.valueOf(ThemeUtils.getThemeColor(this, R.attr.malp_color_text_accent))
        mVolumeIconButtons!!.imageTintList = ColorStateList.valueOf(ThemeUtils.getThemeColor(this, R.attr.malp_color_text_accent))
        mVolumeText!!.text = "$volume%"

        // Update position seekbar & textviews
        mPositionSeekbar!!.max = Math.round(status.trackLength)
        mPositionSeekbar!!.progress = Math.round(status.elapsedTime)
    }

    /**
     * Reacts to new MPD tracks. Shows new track name, album, artist and triggers the fetching
     * of the Fanart.
     *
     * @param track New [MPDTrack] that is playing
     */
    private fun updateMPDCurrentTrack(track: MPDTrack) {
        val title = track.visibleTitle
        val albumName = track.getStringTag(MPDTrack.StringTagTypes.ALBUM)
        val albumArtist = track.getStringTag(MPDTrack.StringTagTypes.ALBUMARTIST)
        val artistName = if (albumArtist.isEmpty()) track.getStringTag(MPDTrack.StringTagTypes.ARTIST) else albumArtist
        mTrackTitle!!.text = title
        mTrackAlbum!!.text = albumName
        mTrackArtist!!.text = artistName
        var lastTrackArtistName: String? = null
        if (mLastTrack != null) {
            val lastAlbumArtist = mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ALBUMARTIST)
            lastTrackArtistName = if (lastAlbumArtist.isEmpty()) mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ARTIST) else lastAlbumArtist
        }
        if (artistName != lastTrackArtistName) {
            // only cancel fanart requests
            MALPRequestQueue.getInstance(applicationContext).cancelAll { request: Request<*>? -> request is FanartImageRequest }
            cancelSwitching()
            mFanartView0!!.setImageBitmap(null)
            mFanartView1!!.setImageBitmap(null)
            mNextFanart = 0
            mCurrentFanart = -1
            mLastTrack = track

            // Initiate the actual Fanart fetching
            mFanartManager!!.syncFanart(mLastTrack, this)
        }
    }

    /**
     * Callback handler to react to changes in server status or a new playing track.
     */
    private class ServerStatusListener internal constructor(fanartActivity: FanartActivity) : MPDStatusChangeHandler() {
        private val mFanartActivity: WeakReference<FanartActivity>
        override fun onNewStatusReady(status: MPDCurrentStatus) {
            val fanartActivity = mFanartActivity.get()
            fanartActivity?.updateMPDStatus(status)
        }

        override fun onNewTrackReady(track: MPDTrack) {
            val fanartActivity = mFanartActivity.get()
            fanartActivity?.updateMPDCurrentTrack(track)
        }

        init {
            mFanartActivity = WeakReference(fanartActivity)
        }
    }

    /**
     * Helper class to switch the views periodically. (Slideshow)
     */
    private inner class ViewSwitchTask : TimerTask() {
        override fun run() {
            runOnUiThread { updateFanartViews() }
        }
    }

    /**
     * This snippet hides the system bars.
     *
     *
     * Set the IMMERSIVE flag.
     * Set the content to appear under the system bars so that the content
     * doesn't resize when the system bars hide and show.
     */
    private fun hideSystemUI() {
        mDecorView!!.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    /**
     * Method to restore the current image after an orientation change.
     */
    private fun restoreFanartView() {
        // Check if a track is available, cancel otherwise
        if (mLastTrack == null || mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ARTIST_MBID).isEmpty()) {
            return
        }
        val mbid = mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ARTIST_MBID)
        val image = mFanartManager!!.getFanartImage(mbid, mCurrentFanart)
        if (image != null) {
            if (mSwitcher!!.displayedChild == 0) {
                mFanartView0!!.setImageBitmap(image)
            } else {
                mFanartView1!!.setImageBitmap(image)
            }
        }
    }

    /**
     * Shows the next image if available. Blank if not.
     */
    private fun updateFanartViews() {
        // Check if a track is available, cancel otherwise
        if (mLastTrack == null || mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ARTIST_MBID).isEmpty()) {
            return
        }
        val mbid = mLastTrack!!.getStringTag(MPDTrack.StringTagTypes.ARTIST_MBID)
        val fanartCount = mFanartManager!!.getFanartCount(mbid)
        if (mSwitcher!!.displayedChild == 0) {
            if (mNextFanart < fanartCount) {
                mCurrentFanart = mNextFanart
                val image = mFanartManager!!.getFanartImage(mbid, mNextFanart)
                mNextFanart = if (image != null) {
                    mFanartView1!!.setImageBitmap(image)

                    // Move pointer with wraparound
                    (mNextFanart + 1) % fanartCount
                } else {
                    return
                }
            }
            mSwitcher!!.displayedChild = 1
        } else {
            if (mNextFanart < fanartCount) {
                mCurrentFanart = mNextFanart
                val image = mFanartManager!!.getFanartImage(mbid, mNextFanart)
                mNextFanart = if (image != null) {
                    mFanartView0!!.setImageBitmap(image)

                    // Move pointer with wraparound
                    (mNextFanart + 1) % fanartCount
                } else {
                    return
                }
            }
            mSwitcher!!.displayedChild = 0
        }
        if (mSwitchTimer == null) {
            startSwitching()
        }
    }

    /**
     * Starts the view switching task that alternates between images.
     */
    private fun startSwitching() {
        mSwitchTimer = Timer()
        mSwitchTimer!!.schedule(ViewSwitchTask(), FANART_SWITCH_TIME.toLong(), FANART_SWITCH_TIME.toLong())
    }

    /**
     * Cancels the view switching task that alternates between images.
     */
    private fun cancelSwitching() {
        if (null != mSwitchTimer) {
            mSwitchTimer!!.cancel()
            mSwitchTimer!!.purge()
            mSwitchTimer = null
        }
    }

    /**
     * Listener class for the volume seekbar.
     */
    private inner class VolumeSeekBarListener : OnSeekBarChangeListener {
        /**
         * Called if the user drags the seekbar to a new position or the seekbar is altered from
         * outside. Just do some seeking, if the action is done by the user.
         *
         * @param seekBar  Seekbar of which the progress was changed.
         * @param progress The new position of the seekbar.
         * @param fromUser If the action was initiated by the user.
         */
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                MPDCommandHandler.setVolume(progress)
                if (progress >= 70) {
                    mVolumeIcon!!.setImageResource(R.drawable.ic_volume_high_black_48dp)
                } else if (progress >= 30) {
                    mVolumeIcon!!.setImageResource(R.drawable.ic_volume_medium_black_48dp)
                } else if (progress > 0) {
                    mVolumeIcon!!.setImageResource(R.drawable.ic_volume_low_black_48dp)
                } else {
                    mVolumeIcon!!.setImageResource(R.drawable.ic_volume_mute_black_48dp)
                }
            }
        }

        /**
         * Called if the user starts moving the seekbar. We do not handle this for now.
         *
         * @param seekBar SeekBar that is used for dragging.
         */
        override fun onStartTrackingTouch(seekBar: SeekBar) {
            // TODO Auto-generated method stub
        }

        /**
         * Called if the user ends moving the seekbar. We do not handle this for now.
         *
         * @param seekBar SeekBar that is used for dragging.
         */
        override fun onStopTrackingTouch(seekBar: SeekBar) {
            // TODO Auto-generated method stub
        }
    }

    /**
     * Listener class for the position seekbar.
     */
    private class PositionSeekbarListener : OnSeekBarChangeListener {
        /**
         * Called if the user drags the seekbar to a new position or the seekbar is altered from
         * outside. Just do some seeking, if the action is done by the user.
         *
         * @param seekBar  Seekbar of which the progress was changed.
         * @param progress The new position of the seekbar.
         * @param fromUser If the action was initiated by the user.
         */
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                // FIXME Check if it is better to just update if user releases the seekbar
                // (network stress)
                MPDCommandHandler.seekSeconds(progress)
            }
        }

        /**
         * Called if the user starts moving the seekbar. We do not handle this for now.
         *
         * @param seekBar SeekBar that is used for dragging.
         */
        override fun onStartTrackingTouch(seekBar: SeekBar) {
            // TODO Auto-generated method stub
        }

        /**
         * Called if the user ends moving the seekbar. We do not handle this for now.
         *
         * @param seekBar SeekBar that is used for dragging.
         */
        override fun onStopTrackingTouch(seekBar: SeekBar) {
            // TODO Auto-generated method stub
        }
    }

    /**
     * Helper function to show the right volume control, requested by the user.
     */
    private fun setVolumeControlSetting() {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val volumeControlView = sharedPref.getString(getString(R.string.pref_volume_controls_key), getString(R.string.pref_volume_control_view_default))
        if (volumeControlView == getString(R.string.pref_volume_control_view_off_key)) {
            mVolumeSeekbarLayout!!.visibility = View.GONE
            mVolumeButtonLayout!!.visibility = View.GONE
        } else if (volumeControlView == getString(R.string.pref_volume_control_view_seekbar_key)) {
            mVolumeSeekbarLayout!!.visibility = View.VISIBLE
            mVolumeButtonLayout!!.visibility = View.GONE
        } else if (volumeControlView == getString(R.string.pref_volume_control_view_buttons_key)) {
            mVolumeSeekbarLayout!!.visibility = View.GONE
            mVolumeButtonLayout!!.visibility = View.VISIBLE
        }
        mVolumeStepSize = sharedPref.getInt(getString(R.string.pref_volume_steps_key), resources.getInteger(R.integer.pref_volume_steps_default))
        mPlusListener!!.setVolumeStepSize(mVolumeStepSize)
        mMinusListener!!.setVolumeStepSize(mVolumeStepSize)
    }

    companion object {
        private val TAG = FanartActivity::class.java.simpleName
        private const val STATE_ARTWORK_POINTER = "artwork_pointer"
        private const val STATE_ARTWORK_POINTER_NEXT = "artwork_pointer_next"
        private const val STATE_LAST_TRACK = "last_track"

        /**
         * Time between two images of the slideshow
         */
        private const val FANART_SWITCH_TIME = 12 * 1000
    }
}