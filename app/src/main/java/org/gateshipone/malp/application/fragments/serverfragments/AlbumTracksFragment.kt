/*
 *  Copyright (C) 2020 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/gateship-one/malp/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.gateshipone.malp.application.fragments.serverfragments

import android.graphics.Bitmap
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.DrawableCompat
import androidx.lifecycle.ViewModelProvider
import org.gateshipone.malp.R
import org.gateshipone.malp.application.adapters.TracksRecyclerViewAdapter
import org.gateshipone.malp.application.artwork.ArtworkManager
import org.gateshipone.malp.application.artwork.ArtworkManager.onNewAlbumImageListener
import org.gateshipone.malp.application.callbacks.AddPathToPlaylist
import org.gateshipone.malp.application.fragments.serverfragments.AlbumTracksFragment
import org.gateshipone.malp.application.listviewitems.GenericViewItemHolder
import org.gateshipone.malp.application.utils.CoverBitmapLoader
import org.gateshipone.malp.application.utils.CoverBitmapLoader.CoverBitmapListener
import org.gateshipone.malp.application.utils.CoverBitmapLoader.IMAGE_TYPE
import org.gateshipone.malp.application.utils.PreferenceHelper
import org.gateshipone.malp.application.utils.PreferenceHelper.LIBRARY_TRACK_CLICK_ACTION
import org.gateshipone.malp.application.utils.ThemeUtils
import org.gateshipone.malp.application.viewmodels.AlbumTracksViewModel
import org.gateshipone.malp.application.viewmodels.AlbumTracksViewModel.AlbumTracksModelFactory
import org.gateshipone.malp.application.viewmodels.GenericViewModel
import org.gateshipone.malp.application.views.MalpRecyclerView
import org.gateshipone.malp.application.views.MalpRecyclerView.RecyclerViewContextMenuInfo
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDCommandHandler
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDQueryHandler
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDAlbum
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDFileEntry
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDTrack

class AlbumTracksFragment : GenericMPDRecyclerFragment<MPDFileEntry?, GenericViewItemHolder?>(), CoverBitmapListener, onNewAlbumImageListener, MalpRecyclerView.OnItemClickListener {
    /**
     * Album definition variables
     */
    private var mAlbum: MPDAlbum? = null
    private var mBitmap: Bitmap? = null
    private var mBitmapLoader: CoverBitmapLoader? = null
    private var mClickAction: LIBRARY_TRACK_CLICK_ACTION? = null
    private var mUseArtistSort = false
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.recycler_list_refresh, container, false)

        // Get the main ListView of this fragment
        mRecyclerView = rootView.findViewById(R.id.recycler_view)

        /* Check if an artistname/albumame was given in the extras */
        val args = arguments
        if (null != args) {
            mAlbum = args.getParcelable(BUNDLE_STRING_EXTRA_ALBUM)
            mBitmap = args.getParcelable(BUNDLE_STRING_EXTRA_BITMAP)
        }

        // Create the needed adapter for the ListView
        mAdapter = TracksRecyclerViewAdapter()

        // Combine the two to a happy couple
        mRecyclerView.adapter = mAdapter
        mRecyclerView.addOnItemClicklistener(this)
        setLinearLayoutManagerAndDecoration()
        registerForContextMenu(mRecyclerView)

        // get swipe layout
        mSwipeRefreshLayout = rootView.findViewById(R.id.refresh_layout)
        // set swipe colors
        mSwipeRefreshLayout.setColorSchemeColors(ThemeUtils.getThemeColor(context, R.attr.colorAccent),
                ThemeUtils.getThemeColor(context, R.attr.colorPrimary))
        // set swipe refresh listener
        mSwipeRefreshLayout.setOnRefreshListener { refreshContent() }
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        mClickAction = PreferenceHelper.getClickAction(sharedPref, context)
        mUseArtistSort = sharedPref.getBoolean(getString(R.string.pref_use_artist_sort_key), resources.getBoolean(R.bool.pref_use_artist_sort_default))
        setHasOptionsMenu(true)
        mBitmapLoader = CoverBitmapLoader(requireContext(), this)
        viewModel.data.observe(viewLifecycleOwner, { model: MutableList<MPDFileEntry?>? -> onDataReady(model) })

        // Return the ready inflated and configured fragment view.
        return rootView
    }

    public override fun getViewModel(): GenericViewModel<MPDFileEntry?> {
        return ViewModelProvider(this, AlbumTracksModelFactory(requireActivity().application, mAlbum, mUseArtistSort)).get(AlbumTracksViewModel::class.java)
    }

    /**
     * Starts the loader to make sure the data is up-to-date after resuming the fragment (from background)
     */
    override fun onResume() {
        super.onResume()
        if (null != mFABCallback) {
            mFABCallback.setupFAB(true, FABOnClickListener())
            mFABCallback.setupToolbar(mAlbum!!.name, false, false, false)
        }
        if (mAlbum != null && mBitmap == null) {
            val rootView = view
            if (rootView != null) {
                requireView().post {
                    val size = rootView.width
                    mBitmapLoader!!.getAlbumImage(mAlbum, false, size, size)
                }
            }
        } else if (mAlbum != null) {
            // Reuse the image passed from the previous fragment
            mFABCallback.setupToolbar(mAlbum!!.name, false, false, true)
            mFABCallback.setupToolbarImage(mBitmap)
            val rootView = view
            if (rootView != null) {
                requireView().post {
                    val size = rootView.width
                    // Image too small
                    if (mBitmap!!.width < size) {
                        mBitmapLoader!!.getAlbumImage(mAlbum, false, size, size)
                    }
                }
            }
        }
        ArtworkManager.getInstance(context).registerOnNewAlbumImageListener(this)
    }

    /**
     * Called when the fragment is hidden. This unregisters the listener for a new album image
     */
    override fun onPause() {
        super.onPause()
        ArtworkManager.getInstance(context).unregisterOnNewAlbumImageListener(this)
    }

    override fun onItemClick(position: Int) {
        when (mClickAction) {
            LIBRARY_TRACK_CLICK_ACTION.ACTION_SHOW_DETAILS -> {

                // Open song details dialog
                val songDetailsDialog = SongDetailsDialog.createDialog((mAdapter.getItem(position) as MPDTrack?)!!, false)
                songDetailsDialog.show((context as AppCompatActivity?)!!.supportFragmentManager, "SongDetails")
            }
            LIBRARY_TRACK_CLICK_ACTION.ACTION_ADD_SONG -> {
                enqueueTrack(position)
            }
            LIBRARY_TRACK_CLICK_ACTION.ACTION_ADD_SONG_AT_START -> {
                prependTrack(position)
            }
            LIBRARY_TRACK_CLICK_ACTION.ACTION_PLAY_SONG -> {
                play(position)
            }
            LIBRARY_TRACK_CLICK_ACTION.ACTION_PLAY_SONG_NEXT -> {
                playNext(position)
            }
        }
    }

    /**
     * Create the context menu.
     */
    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = requireActivity().menuInflater
        inflater.inflate(R.menu.context_menu_track, menu)
    }

    /**
     * Hook called when an menu item in the context menu is selected.
     *
     * @param item The menu item that was selected.
     * @return True if the hook was consumed here.
     */
    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as RecyclerViewContextMenuInfo
                ?: return super.onContextItemSelected(item)
        val itemId = item.itemId
        if (itemId == R.id.action_song_enqueue) {
            enqueueTrack(info.position)
            return true
        } else if (itemId == R.id.action_song_play) {
            play(info.position)
            return true
        } else if (itemId == R.id.action_song_play_next) {
            playNext(info.position)
            return true
        } else if (itemId == R.id.action_song_enqueue_at_start) {
            prependTrack(info.position)
            return true
        } else if (itemId == R.id.action_add_to_saved_playlist) {
            // open dialog in order to save the current playlist as a playlist in the mediastore
            val choosePlaylistDialog = ChoosePlaylistDialog.newInstance(true)
            choosePlaylistDialog.setCallback(AddPathToPlaylist(mAdapter.getItem(info.position), activity))
            choosePlaylistDialog.show((context as AppCompatActivity?)!!.supportFragmentManager, "ChoosePlaylistDialog")
            return true
        } else if (itemId == R.id.action_show_details) {
            // Open song details dialog
            val songDetailsDialog = SongDetailsDialog.createDialog((mAdapter.getItem(info.position) as MPDTrack?)!!, false)
            songDetailsDialog.show((context as AppCompatActivity?)!!.supportFragmentManager, "SongDetails")
            return true
        }
        return super.onContextItemSelected(item)
    }

    /**
     * Initialize the options menu.
     * Be sure to call [.setHasOptionsMenu] before.
     *
     * @param menu         The container for the custom options menu.
     * @param menuInflater The inflater to instantiate the layout.
     */
    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.fragment_menu_album_tracks, menu)

        // get tint color
        val tintColor = ThemeUtils.getThemeColor(context, R.attr.malp_color_text_accent)
        var drawable = menu.findItem(R.id.action_add_album).icon
        drawable = DrawableCompat.wrap(drawable!!)
        DrawableCompat.setTint(drawable, tintColor)
        menu.findItem(R.id.action_add_album).icon = drawable
        if (!mAlbum!!.mbid.isEmpty()) {
            // Disable legacy feature to remove filter criteria for album tracks if an MBID is
            // available. Albums tagged with a MBID can be considered shown correctly.
            menu.findItem(R.id.action_show_all_tracks).isVisible = false
        }
        super.onCreateOptionsMenu(menu, menuInflater)
    }

    /**
     * Hook called when an menu item in the options menu is selected.
     *
     * @param item The menu item that was selected.
     * @return True if the hook was consumed here.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId
        if (itemId == R.id.action_reset_artwork) {
            if (null != mFABCallback) {
                mFABCallback.setupToolbar(mAlbum!!.name, false, false, false)
            }
            ArtworkManager.getInstance(context).resetAlbumImage(mAlbum)
            return true
        } else if (itemId == R.id.action_add_album) {
            enqueueAlbum()
            return true
        } else if (itemId == R.id.action_show_all_tracks) {
            mAlbum!!.mbid = ""
            mAlbum!!.artistName = ""
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        // Do not save the bitmap for later use (too big for binder)
        val args = arguments
        if (args != null) {
            requireArguments().remove(BUNDLE_STRING_EXTRA_BITMAP)
        }
        super.onSaveInstanceState(savedInstanceState)
    }

    private fun enqueueTrack(index: Int) {
        val entry = mAdapter.getItem(index)!!
        MPDQueryHandler.addPath(entry.path)
    }

    private fun prependTrack(index: Int) {
        val entry = mAdapter.getItem(index)!!
        MPDQueryHandler.addPathAtStart(entry.path)
    }

    private fun play(index: Int) {
        val track = mAdapter.getItem(index) as MPDTrack
        MPDQueryHandler.playSong(track.path)
    }

    private fun playNext(index: Int) {
        val track = mAdapter.getItem(index) as MPDTrack
        MPDQueryHandler.playSongNext(track.path)
    }

    private fun enqueueAlbum() {
        if (mUseArtistSort) {
            MPDQueryHandler.addArtistSortAlbum(mAlbum!!.name, mAlbum!!.artistSortName, mAlbum!!.mbid)
        } else {
            MPDQueryHandler.addArtistAlbum(mAlbum!!.name, mAlbum!!.artistName, mAlbum!!.mbid)
        }
    }

    override fun receiveBitmap(bm: Bitmap?, type: IMAGE_TYPE?) {
        if (type === IMAGE_TYPE.ALBUM_IMAGE && null != mFABCallback && bm != null) {
            val activity = activity
            activity?.runOnUiThread {
                mFABCallback.setupToolbar(mAlbum!!.name, false, false, true)
                mFABCallback.setupToolbarImage(bm)
                requireArguments().putParcelable(BUNDLE_STRING_EXTRA_BITMAP, bm)
            }
        }
    }

    override fun newAlbumImage(album: MPDAlbum) {
        if (album == mAlbum) {
            val width = requireView().measuredWidth
            mBitmapLoader!!.getAlbumImage(mAlbum, false, width, width)
        }
    }

    private inner class FABOnClickListener : View.OnClickListener {
        override fun onClick(v: View) {
            MPDCommandHandler.setRandom(false)
            MPDCommandHandler.setRepeat(false)
            if (mUseArtistSort) {
                MPDQueryHandler.playArtistSortAlbum(mAlbum!!.name, mAlbum!!.artistSortName, mAlbum!!.mbid)
            } else {
                MPDQueryHandler.playArtistAlbum(mAlbum!!.name, mAlbum!!.artistName, mAlbum!!.mbid)
            }
        }
    }

    companion object {
        val TAG = AlbumTracksFragment::class.java.simpleName

        /**
         * Parameters for bundled extra arguments for this fragment. Necessary to define which album to
         * retrieve from the MPD server.
         */
        private const val BUNDLE_STRING_EXTRA_ALBUM = "album"
        private const val BUNDLE_STRING_EXTRA_BITMAP = "bitmap"
        fun newInstance(album: MPDAlbum, bitmap: Bitmap?): AlbumTracksFragment {
            val args = Bundle()
            args.putParcelable(BUNDLE_STRING_EXTRA_ALBUM, album)
            args.putParcelable(BUNDLE_STRING_EXTRA_BITMAP, bitmap)
            val fragment = AlbumTracksFragment()
            fragment.arguments = args
            return fragment
        }
    }
}