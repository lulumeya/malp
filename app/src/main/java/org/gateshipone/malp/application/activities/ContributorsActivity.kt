/*
 *  Copyright (C) 2020 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/gateship-one/malp/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.gateshipone.malp.application.activities

import android.os.Bundle
import android.widget.ListView
import android.widget.SimpleAdapter
import org.gateshipone.malp.R
import org.gateshipone.malp.application.utils.ThemeUtils
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDException.MPDConnectionException
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDException.MPDServerException
import java.util.*

class ContributorsActivity : GenericActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contributors)
        window.statusBarColor = ThemeUtils.getThemeColor(this, R.attr.malp_color_primary_dark)
        val contributors = findViewById<ListView>(R.id.contributors_listview)
        val contributors_names = resources.getStringArray(R.array.contributors)
        val contributors_types = resources.getStringArray(R.array.contributors_type)
        val contributors_list: MutableList<Map<String, String?>> = ArrayList()
        var map: MutableMap<String, String?>
        for (i in contributors_names.indices) {
            map = HashMap()
            map[CONTRIBUTOR_NAME_KEY] = contributors_names[i]
            map[CONTRIBUTOR_TYPE_KEY] = contributors_types[i]
            contributors_list.add(map)
        }
        val adapter = SimpleAdapter(this, contributors_list, R.layout.listview_item, arrayOf(CONTRIBUTOR_NAME_KEY, CONTRIBUTOR_TYPE_KEY), intArrayOf(R.id.item_title, R.id.item_subtitle))
        contributors.adapter = adapter
    }

    override fun onConnected() {}
    override fun onDisconnected() {}
    override fun onMPDError(e: MPDServerException?) {}
    override fun onMPDConnectionError(e: MPDConnectionException?) {}

    companion object {
        private const val CONTRIBUTOR_NAME_KEY = "name"
        private const val CONTRIBUTOR_TYPE_KEY = "type"
    }
}