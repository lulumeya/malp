/*
 *  Copyright (C) 2021 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/gateship-one/malp/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gateshipone.malp.application.retrofit

import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query

interface KbsService {

    @GET("https://onair.kbs.co.kr/index.html?sname=onair&stype=live")
    suspend fun getFm1Url(@Query("ch_code") code: Int): ResponseBody

    @GET("http://pckong.kbs.co.kr/auth/api_auth.php?url=aHR0cDovL3Bja29uZy5rYnMuY28ua3IvbWtvbmcvYXBpLnBocD9hcGlfbmFtZT1saXZlX3N0cmVhbSZkZXZpY2VfdHlwZT1pcGhvbmUmYmV0YT0wJmlzX2JvcmE9TiZjaGFubmVsX2NvZGU9MjQmcmVxdHM9MjAyMTA0MzAwOSZhdXRoY29kZT05MzRDMkFBOEI2MkM0QTRFN0Y1Qjg4MzUyRUI4MTk5NzNFNjE3NjJBOUI3ODEzODdCRjNFQjhFRjI4ODI3QUU5")
    suspend fun getFm1UrlByKong(): KongResponse
}