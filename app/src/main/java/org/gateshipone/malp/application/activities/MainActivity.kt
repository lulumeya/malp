/*
 *  Copyright (C) 2020 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/gateship-one/malp/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.gateshipone.malp.application.activities

import android.content.DialogInterface
import android.graphics.Bitmap
import android.os.Bundle
import android.preference.PreferenceManager
import android.transition.Slide
import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.view.ViewCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import org.gateshipone.malp.R
import org.gateshipone.malp.application.adapters.CurrentPlaylistAdapter
import org.gateshipone.malp.application.callbacks.*
import org.gateshipone.malp.application.fragments.*
import org.gateshipone.malp.application.fragments.SettingsFragment.OnArtworkSettingsRequestedCallback
import org.gateshipone.malp.application.fragments.serverfragments.*
import org.gateshipone.malp.application.fragments.serverfragments.ArtistsFragment.ArtistSelectedCallback
import org.gateshipone.malp.application.fragments.serverfragments.FilesFragment.FilesCallback
import org.gateshipone.malp.application.fragments.serverfragments.MyMusicTabsFragment.DEFAULTTAB
import org.gateshipone.malp.application.utils.ThemeUtils
import org.gateshipone.malp.application.views.CurrentPlaylistView
import org.gateshipone.malp.application.views.NowPlayingView
import org.gateshipone.malp.application.views.NowPlayingView.NowPlayingDragStatusReceiver
import org.gateshipone.malp.application.views.NowPlayingView.NowPlayingDragStatusReceiver.DRAG_STATUS
import org.gateshipone.malp.application.views.NowPlayingView.NowPlayingDragStatusReceiver.VIEW_SWITCHER_STATUS
import org.gateshipone.malp.mpdservice.ConnectionManager
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDQueryHandler
import org.gateshipone.malp.mpdservice.handlers.serverhandler.MPDStateMonitoringHandler
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDException.MPDConnectionException
import org.gateshipone.malp.mpdservice.mpdprotocol.MPDException.MPDServerException
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDAlbum
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDArtist
import org.gateshipone.malp.mpdservice.mpdprotocol.mpdobjects.MPDTrack
import org.gateshipone.malp.mpdservice.profilemanagement.MPDProfileManager
import org.gateshipone.malp.mpdservice.profilemanagement.MPDServerProfile

class MainActivity : GenericActivity(), NavigationView.OnNavigationItemSelectedListener, AlbumCallback, ArtistSelectedCallback, ProfileManageCallbacks, PlaylistCallback, NowPlayingDragStatusReceiver,
    FilesCallback, FABFragmentCallback, OnArtworkSettingsRequestedCallback {
    private var mNowPlayingDragStatus: DRAG_STATUS? = null
    private var mSavedNowPlayingDragStatus: DRAG_STATUS? = null

    enum class REQUESTEDVIEW {
        NONE, NOWPLAYING, SETTINGS
    }

    private var mDrawerToggle: ActionBarDrawerToggle? = null
    private var mNowPlayingViewSwitcherStatus: VIEW_SWITCHER_STATUS? = null
    private var mSavedNowPlayingViewSwitcherStatus: VIEW_SWITCHER_STATUS? = null
    private var mHeaderImageActive = false
    private var mUseArtistSort = false
    private lateinit var mFAB: FloatingActionButton
    private var mShowNPV = false
    override fun onCreate(savedInstanceState: Bundle?) {
        var switchToSettings = false

        // restore drag state
        if (savedInstanceState != null) {
            mSavedNowPlayingDragStatus = DRAG_STATUS.values()[savedInstanceState.getInt(MAINACTIVITY_SAVED_INSTANCE_NOW_PLAYING_DRAG_STATUS)]
            mSavedNowPlayingViewSwitcherStatus = VIEW_SWITCHER_STATUS.values()[savedInstanceState.getInt(MAINACTIVITY_SAVED_INSTANCE_NOW_PLAYING_VIEW_SWITCHER_CURRENT_VIEW)]
        } else {
            // if no savedInstanceState is present the activity is started for the first time so check the intent
            val intent = intent
            if (intent != null) {
                // odyssey was opened by widget or notification
                val extras = intent.extras
                if (extras != null) {
                    val requestedView = REQUESTEDVIEW.values()[extras.getInt(MAINACTIVITY_INTENT_EXTRA_REQUESTEDVIEW, REQUESTEDVIEW.NONE.ordinal)]
                    when (requestedView) {
                        REQUESTEDVIEW.NONE -> {
                        }
                        REQUESTEDVIEW.NOWPLAYING -> mShowNPV = true
                        REQUESTEDVIEW.SETTINGS -> switchToSettings = true
                    }
                }
            }
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // restore elevation behaviour as pre 24 support lib
        val layout = findViewById<AppBarLayout>(R.id.appbar)
        layout.stateListAnimator = null
        ViewCompat.setElevation(layout, 0f)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        // enable back navigation
        val actionBar = supportActionBar
        if (actionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer != null) {
            mDrawerToggle = ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
            drawer.addDrawerListener(mDrawerToggle!!)
            mDrawerToggle!!.syncState()
        }
        var navId = if (switchToSettings) R.id.nav_app_settings else defaultViewID
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this)
            navigationView.setCheckedItem(navId)
        }
        mFAB = findViewById(R.id.andrompd_play_button)
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        mUseArtistSort = sharedPref.getBoolean(getString(R.string.pref_use_artist_sort_key), resources.getBoolean(R.bool.pref_use_artist_sort_default))
        registerForContextMenu(findViewById(R.id.main_listview))
        if (MPDProfileManager.getInstance(this).profiles.size == 0) {
            navId = R.id.nav_profiles
            val builder = AlertDialog.Builder(this)
            builder.setTitle(resources.getString(R.string.welcome_dialog_title))
            builder.setMessage(resources.getString(R.string.welcome_dialog_text))
            builder.setPositiveButton(R.string.dialog_action_ok) { dialog: DialogInterface?, id: Int -> }
            val dialog = builder.create()
            dialog.show()
        }
        if (findViewById<View?>(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return
            }
            val fragment: Fragment
            val fragmentTag: String
            if (navId == R.id.nav_saved_playlists) {
                fragment = SavedPlaylistsFragment.newInstance()
                fragmentTag = SavedPlaylistsFragment.TAG
            } else if (navId == R.id.nav_files) {
                fragment = FilesFragment.newInstance("")
                fragmentTag = FilesFragment.TAG
            } else if (navId == R.id.nav_profiles) {
                fragment = ProfilesFragment.newInstance()
                fragmentTag = ProfilesFragment.TAG
            } else if (navId == R.id.nav_app_settings) {
                fragment = SettingsFragment.newInstance()
                fragmentTag = SettingsFragment.TAG
            } else if (navId == R.id.nav_search) {
                fragment = SearchFragment.newInstance()
                fragmentTag = SearchFragment.TAG
            } else if (navId == R.id.nav_library) {
                fragment = MyMusicTabsFragment.newInstance(defaultTab)
                fragmentTag = MyMusicTabsFragment.TAG
            } else {
                fragment = MyMusicTabsFragment.newInstance(defaultTab)
                fragmentTag = MyMusicTabsFragment.TAG
            }
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, fragment, fragmentTag)
            transaction.commit()
        }
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val fragmentManager = supportFragmentManager
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else if (mNowPlayingDragStatus == DRAG_STATUS.DRAGGED_UP) {
            val nowPlayingView = findViewById<NowPlayingView>(R.id.now_playing_layout)
            if (nowPlayingView != null) {
                val coordinatorLayout = findViewById<View>(R.id.main_coordinator_layout)
                coordinatorLayout.visibility = View.VISIBLE
                nowPlayingView.minimize()
            }
        } else {
            super.onBackPressed()

            // enable navigation bar when backstack empty
            if (fragmentManager.backStackEntryCount == 0) {
                mDrawerToggle!!.isDrawerIndicatorEnabled = true
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val fragmentManager = supportFragmentManager
        if (item.itemId == android.R.id.home) {
            if (fragmentManager.backStackEntryCount > 0) {
                onBackPressed()
            } else {
                // back stack empty so enable navigation drawer
                mDrawerToggle!!.isDrawerIndicatorEnabled = true
                if (mDrawerToggle!!.onOptionsItemSelected(item)) {
                    return true
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo)
        if (v.id == R.id.main_listview && mNowPlayingDragStatus == DRAG_STATUS.DRAGGED_UP) {
            val position = (menuInfo as AdapterContextMenuInfo).position
            val inflater = menuInflater
            inflater.inflate(R.menu.context_menu_current_playlist_track, menu)

            // Check if the menu is created for the currently playing song. If this is the case, do not show play as next item.
            val status = MPDStateMonitoringHandler.getHandler().lastStatus
            if (status != null && position == status.currentSongIndex) {
                menu.findItem(R.id.action_song_play_next).isVisible = false
            }
            val currentPlaylistView = findViewById<CurrentPlaylistView>(R.id.now_playing_playlist)
            if (currentPlaylistView.getItemViewType(position) == CurrentPlaylistAdapter.VIEW_TYPES.TYPE_SECTION_TRACK_ITEM) {
                menu.findItem(R.id.action_remove_album).isVisible = true
            }
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val menuInfo = item.menuInfo ?: return super.onContextItemSelected(item)

        // we have two types of adapter context menuinfo classes so we have to make sure the current item contains the correct type of menuinfo
        if (menuInfo is AdapterContextMenuInfo) {
            val info = menuInfo
            val currentPlaylistView = findViewById<CurrentPlaylistView>(R.id.now_playing_playlist)
            if (currentPlaylistView != null && mNowPlayingDragStatus == DRAG_STATUS.DRAGGED_UP) {
                val track = currentPlaylistView.getItem(info.position) as MPDTrack
                val itemId = item.itemId
                if (itemId == R.id.action_song_play_next) {
                    MPDQueryHandler.playIndexAsNext(info.position)
                    return true
                } else if (itemId == R.id.action_add_to_saved_playlist) {
                    // open dialog in order to save the current playlist as a playlist in the mediastore
                    val choosePlaylistDialog = ChoosePlaylistDialog.newInstance(true)
                    choosePlaylistDialog.setCallback(AddPathToPlaylist(track, this))
                    choosePlaylistDialog.show(supportFragmentManager, "ChoosePlaylistDialog")
                    return true
                } else if (itemId == R.id.action_remove_song) {
                    MPDQueryHandler.removeSongFromCurrentPlaylist(info.position)
                    return true
                } else if (itemId == R.id.action_remove_album) {
                    currentPlaylistView.removeAlbumFrom(info.position)
                    return true
                } else if (itemId == R.id.action_show_artist) {
                    if (mUseArtistSort) {
                        onArtistSelected(MPDArtist(track.getStringTag(MPDTrack.StringTagTypes.ARTISTSORT)), null)
                    } else {
                        onArtistSelected(MPDArtist(track.getStringTag(MPDTrack.StringTagTypes.ARTIST)), null)
                    }
                    return true
                } else if (itemId == R.id.action_show_album) {
                    val tmpAlbum = track.album
                    onAlbumSelected(tmpAlbum, null)
                    return true
                } else if (itemId == R.id.action_show_details) {
                    // Open song details dialog
                    val songDetailsDialog = SongDetailsDialog.createDialog(track, true)
                    songDetailsDialog.show(supportFragmentManager, "SongDetails")
                    return true
                }
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId
        val coordinatorLayout = findViewById<View>(R.id.main_coordinator_layout)
        coordinatorLayout.visibility = View.VISIBLE
        val nowPlayingView = findViewById<NowPlayingView>(R.id.now_playing_layout)
        nowPlayingView?.minimize()
        val fragmentManager = supportFragmentManager

        // clear backstack
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        var fragment: Fragment? = null
        var fragmentTag: String? = ""
        if (id == R.id.nav_library) {
            fragment = MyMusicTabsFragment.newInstance(defaultTab)
            fragmentTag = MyMusicTabsFragment.TAG
        } else if (id == R.id.nav_saved_playlists) {
            fragment = SavedPlaylistsFragment.newInstance()
            fragmentTag = SavedPlaylistsFragment.TAG
        } else if (id == R.id.nav_files) {
            fragment = FilesFragment.newInstance("")
            fragmentTag = FilesFragment.TAG
        } else if (id == R.id.nav_search) {
            fragment = SearchFragment.newInstance()
            fragmentTag = SearchFragment.TAG
        } else if (id == R.id.nav_profiles) {
            fragment = ProfilesFragment.newInstance()
            fragmentTag = ProfilesFragment.TAG
        } else if (id == R.id.nav_app_settings) {
            fragment = SettingsFragment.newInstance()
            fragmentTag = SettingsFragment.TAG
        } else if (id == R.id.nav_server_properties) {
            fragment = ServerPropertiesFragment.newInstance()
            fragmentTag = ServerPropertiesFragment.TAG
        } else if (id == R.id.nav_information) {
            fragment = InformationSettingsFragment.newInstance()
            fragmentTag = InformationSettingsFragment::class.java.simpleName
        }
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)

        // Do the actual fragment transaction
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment!!, fragmentTag)
        transaction.commit()
        return true
    }

    override fun onResume() {
        super.onResume()
        val nowPlayingView = findViewById<NowPlayingView>(R.id.now_playing_layout)
        if (nowPlayingView != null) {
            nowPlayingView.registerDragStatusReceiver(this)

            /*
             * Check if the activity got an extra in its intend to show the nowplayingview directly.
             * If yes then pre set the dragoffset of the draggable helper.
             */if (mShowNPV) {
                nowPlayingView.setDragOffset(0.0f)

                // check preferences if the playlist should be shown
                val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
                val showPlaylist = sharedPref.getBoolean(getString(R.string.pref_npv_show_playlist_key), resources.getBoolean(R.bool.pref_npv_show_playlist_default))
                if (showPlaylist) {
                    mNowPlayingViewSwitcherStatus = VIEW_SWITCHER_STATUS.PLAYLIST_VIEW
                    nowPlayingView.setViewSwitcherStatus(mNowPlayingViewSwitcherStatus)
                }
                mShowNPV = false
            } else {
                // set drag status
                if (mSavedNowPlayingDragStatus == DRAG_STATUS.DRAGGED_UP) {
                    nowPlayingView.setDragOffset(0.0f)
                } else if (mSavedNowPlayingDragStatus == DRAG_STATUS.DRAGGED_DOWN) {
                    nowPlayingView.setDragOffset(1.0f)
                }
                mSavedNowPlayingDragStatus = null

                // set view switcher status
                if (mSavedNowPlayingViewSwitcherStatus != null) {
                    nowPlayingView.setViewSwitcherStatus(mSavedNowPlayingViewSwitcherStatus)
                    mNowPlayingViewSwitcherStatus = mSavedNowPlayingViewSwitcherStatus
                }
                mSavedNowPlayingViewSwitcherStatus = null
            }
            nowPlayingView.onResume()
        }
    }

    override fun onPause() {
        super.onPause()
        val nowPlayingView = findViewById<NowPlayingView>(R.id.now_playing_layout)
        if (nowPlayingView != null) {
            nowPlayingView.registerDragStatusReceiver(null)
            nowPlayingView.onPause()
        }
    }

    override fun onConnected() {
        setNavbarHeader(ConnectionManager.getInstance(applicationContext).profileName)
    }

    override fun onDisconnected() {
        setNavbarHeader(getString(R.string.app_name_nice))
    }

    override fun onMPDError(e: MPDServerException?) {
        val layout = findViewById<View>(R.id.drawer_layout)
        if (layout != null) {
            val errorText = getString(R.string.snackbar_mpd_server_error_format, e!!.errorCode, e.commandOffset, e.serverMessage)
            val sb = Snackbar.make(layout, errorText, Snackbar.LENGTH_LONG)

            // style the snackbar text
            val sbText = sb.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
            sbText.setTextColor(ThemeUtils.getThemeColor(this, R.attr.malp_color_text_accent))
            sb.show()
        }
    }

    override fun onMPDConnectionError(e: MPDConnectionException?) {
        val layout = findViewById<View>(R.id.drawer_layout)
        if (layout != null) {
            val errorText = getString(R.string.snackbar_mpd_connection_error_format, e!!.error)
            val sb = Snackbar.make(layout, errorText, Snackbar.LENGTH_LONG)

            // style the snackbar text
            val sbText = sb.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
            sbText.setTextColor(ThemeUtils.getThemeColor(this, R.attr.malp_color_text_accent))
            sb.show()
        }
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)

        // save drag status of the nowplayingview
        savedInstanceState.putInt(MAINACTIVITY_SAVED_INSTANCE_NOW_PLAYING_DRAG_STATUS, mNowPlayingDragStatus!!.ordinal)

        // save the cover/playlist view status of the nowplayingview
        savedInstanceState.putInt(MAINACTIVITY_SAVED_INSTANCE_NOW_PLAYING_VIEW_SWITCHER_CURRENT_VIEW, mNowPlayingViewSwitcherStatus!!.ordinal)
    }

    override fun onAlbumSelected(album: MPDAlbum, bitmap: Bitmap?) {
        if (mNowPlayingDragStatus == DRAG_STATUS.DRAGGED_UP) {
            val nowPlayingView = findViewById<NowPlayingView>(R.id.now_playing_layout)
            if (nowPlayingView != null) {
                val coordinatorLayout = findViewById<View>(R.id.main_coordinator_layout)
                coordinatorLayout.visibility = View.VISIBLE
                nowPlayingView.minimize()
            }
        }

        // Create fragment and give it an argument for the selected article
        val newFragment = AlbumTracksFragment.newInstance(album, bitmap)
        val transaction = supportFragmentManager.beginTransaction()
        // Replace whatever is in the fragment_container view with this
        // fragment,
        // and add the transaction to the back stack so the user can navigate
        // back
        newFragment.enterTransition = Slide(Gravity.BOTTOM)
        newFragment.exitTransition = Slide(Gravity.TOP)
        transaction.replace(R.id.fragment_container, newFragment, AlbumTracksFragment.TAG)
        transaction.addToBackStack("AlbumTracksFragment")
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setCheckedItem(R.id.nav_library)

        // Commit the transaction
        transaction.commit()
    }

    override fun onArtistSelected(artist: MPDArtist, bitmap: Bitmap?) {
        if (mNowPlayingDragStatus == DRAG_STATUS.DRAGGED_UP) {
            val nowPlayingView = findViewById<NowPlayingView>(R.id.now_playing_layout)
            if (nowPlayingView != null) {
                val coordinatorLayout = findViewById<View>(R.id.main_coordinator_layout)
                coordinatorLayout.visibility = View.VISIBLE
                nowPlayingView.minimize()
            }
        }

        // Create fragment and give it an argument for the selected article
        val newFragment = ArtistAlbumsFragment.newInstance(artist, bitmap)
        val transaction = supportFragmentManager.beginTransaction()
        newFragment.enterTransition = Slide(Gravity.BOTTOM)
        newFragment.exitTransition = Slide(Gravity.TOP)
        // Replace whatever is in the fragment_container view with this
        // fragment,
        // and add the transaction to the back stack so the user can navigate
        // back
        transaction.replace(R.id.fragment_container, newFragment, ArtistAlbumsFragment.TAG)
        transaction.addToBackStack("ArtistAlbumsFragment")
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setCheckedItem(R.id.nav_library)

        // Commit the transaction
        transaction.commit()
    }

    override fun onStatusChanged(status: DRAG_STATUS) {
        mNowPlayingDragStatus = status
        if (status == DRAG_STATUS.DRAGGED_UP) {
            val coordinatorLayout = findViewById<View>(R.id.main_coordinator_layout)
            coordinatorLayout.visibility = View.INVISIBLE
        }
    }

    override fun onDragPositionChanged(pos: Float) {
        if (mHeaderImageActive) {
            // Get the primary color of the active theme from the helper.
            var newColor = ThemeUtils.getThemeColor(this, R.attr.colorPrimaryDark)

            // Calculate the offset depending on the floating point position (0.0-1.0 of the view)
            // Shift by 24 bit to set it as the A from ARGB and set all remaining 24 bits to 1 to
            val alphaOffset = 255 - (255.0 * pos).toInt() shl 24 or 0xFFFFFF
            // and with this mask to set the new alpha value.
            newColor = newColor and alphaOffset
            window.statusBarColor = newColor
        }
    }

    override fun onSwitchedViews(view: VIEW_SWITCHER_STATUS) {
        mNowPlayingViewSwitcherStatus = view
    }

    override fun onStartDrag() {
        val coordinatorLayout = findViewById<View>(R.id.main_coordinator_layout)
        coordinatorLayout.visibility = View.VISIBLE
    }

    override fun editProfile(profile: MPDServerProfile?) {
        var profile: MPDServerProfile? = profile
        if (null == profile) {
            profile = MPDServerProfile(getString(R.string.fragment_profile_default_name), true)
            ConnectionManager.getInstance(
                applicationContext
            ).addProfile(profile, this)
        }

        // Create fragment and give it an argument for the selected article
        val newFragment = EditProfileFragment.newInstance(profile)
        val transaction = supportFragmentManager.beginTransaction()
        newFragment.enterTransition = Slide(GravityCompat.getAbsoluteGravity(GravityCompat.END, resources.configuration.layoutDirection))
        newFragment.exitTransition = Slide(GravityCompat.getAbsoluteGravity(GravityCompat.START, resources.configuration.layoutDirection))
        // Replace whatever is in the fragment_container view with this
        // fragment,
        // and add the transaction to the back stack so the user can navigate
        // back
        transaction.replace(R.id.fragment_container, newFragment, EditProfileFragment.TAG)
        transaction.addToBackStack("EditProfileFragment")


        // Commit the transaction
        transaction.commit()
    }

    override fun openPlaylist(name: String) {
        // Create fragment and give it an argument for the selected article
        val newFragment = PlaylistTracksFragment.newInstance(name)
        val transaction = supportFragmentManager.beginTransaction()
        newFragment.enterTransition = Slide(GravityCompat.getAbsoluteGravity(GravityCompat.END, resources.configuration.layoutDirection))
        newFragment.exitTransition = Slide(GravityCompat.getAbsoluteGravity(GravityCompat.START, resources.configuration.layoutDirection))
        // Replace whatever is in the fragment_container view with this
        // fragment,
        // and add the transaction to the back stack so the user can navigate
        // back
        transaction.replace(R.id.fragment_container, newFragment)
        transaction.addToBackStack("PlaylistTracksFragment")

        // Commit the transaction
        transaction.commit()
    }

    override fun setupFAB(active: Boolean, listener: View.OnClickListener?) {
        mFAB = findViewById(R.id.andrompd_play_button)
        if (active) {
            mFAB.show()
        } else {
            mFAB.hide()
        }
        mFAB.setOnClickListener(listener)
    }

    override fun setupToolbar(
        title: String, scrollingEnabled: Boolean,
        drawerIndicatorEnabled: Boolean, showImage: Boolean
    ) {
        // set drawer state
        mDrawerToggle!!.isDrawerIndicatorEnabled = drawerIndicatorEnabled
        val collapsingImageLayout = findViewById<RelativeLayout>(R.id.appbar_image_layout)
        val collapsingImage = findViewById<ImageView>(R.id.collapsing_image)
        if (collapsingImage != null) {
            if (showImage) {
                collapsingImageLayout.visibility = View.VISIBLE
                mHeaderImageActive = true

                // Get the primary color of the active theme from the helper.
                var newColor = ThemeUtils.getThemeColor(this, R.attr.colorPrimaryDark)

                // Calculate the offset depending on the floating point position (0.0-1.0 of the view)
                // Shift by 24 bit to set it as the A from ARGB and set all remaining 24 bits to 1 to
                val alphaOffset = 255 - (255.0 * if (mNowPlayingDragStatus == DRAG_STATUS.DRAGGED_UP) 0.0 else 1.0).toInt() shl 24 or 0xFFFFFF
                // and with this mask to set the new alpha value.
                newColor = newColor and alphaOffset
                window.statusBarColor = newColor
            } else {
                collapsingImageLayout.visibility = View.GONE
                mHeaderImageActive = false

                // Get the primary color of the active theme from the helper.
                window.statusBarColor = ThemeUtils.getThemeColor(this, R.attr.colorPrimaryDark)
            }
        } else {
            // If in portrait mode (no collapsing image exists), the status bar also needs dark coloring
            mHeaderImageActive = false

            // Get the primary color of the active theme from the helper.
            window.statusBarColor = ThemeUtils.getThemeColor(this, R.attr.colorPrimaryDark)
        }
        // set scrolling behaviour
        val toolbar = findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar)
        val params = toolbar.layoutParams as AppBarLayout.LayoutParams
        params.height = -1
        if (scrollingEnabled && !showImage) {
            toolbar.isTitleEnabled = false
            setTitle(title)
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL + AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS_COLLAPSED + AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
        } else if (!scrollingEnabled && showImage && collapsingImage != null) {
            toolbar.isTitleEnabled = true
            toolbar.title = title
            params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED + AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
        } else {
            toolbar.isTitleEnabled = false
            setTitle(title)
            params.scrollFlags = 0
        }
    }

    override fun getNowPlayingDragStatus(): DRAG_STATUS {
        return mNowPlayingDragStatus!!
    }

    override fun setupToolbarImage(bm: Bitmap) {
        val collapsingImage = findViewById<ImageView>(R.id.collapsing_image)
        if (collapsingImage != null) {
            collapsingImage.setImageBitmap(bm)

            // FIXME DIRTY HACK: Manually fix the toolbar size to the screen width
            val toolbar = findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar)
            val params = toolbar.layoutParams as AppBarLayout.LayoutParams
            params.height = window.decorView.measuredWidth

            // Always expand the toolbar to show the complete image
            val appbar = findViewById<AppBarLayout>(R.id.appbar)
            appbar.setExpanded(true, false)
        }
    }

    override fun openPath(path: String) {
        // Create fragment and give it an argument for the selected directory
        val newFragment = FilesFragment.newInstance(path)
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        newFragment.enterTransition = Slide(GravityCompat.getAbsoluteGravity(GravityCompat.END, resources.configuration.layoutDirection))
        newFragment.exitTransition = Slide(GravityCompat.getAbsoluteGravity(GravityCompat.START, resources.configuration.layoutDirection))
        transaction.addToBackStack("FilesFragment$path")
        transaction.replace(R.id.fragment_container, newFragment)

        // Commit the transaction
        transaction.commit()
    }

    override fun showAlbumsForPath(path: String) {
        if (mNowPlayingDragStatus == DRAG_STATUS.DRAGGED_UP) {
            val nowPlayingView = findViewById<NowPlayingView>(R.id.now_playing_layout)
            if (nowPlayingView != null) {
                val coordinatorLayout = findViewById<View>(R.id.main_coordinator_layout)
                coordinatorLayout.visibility = View.VISIBLE
                nowPlayingView.minimize()
            }
        }
        // Create fragment and give it an argument for the selected article
        val newFragment = AlbumsFragment.newInstance(path)
        val transaction = supportFragmentManager.beginTransaction()
        newFragment.enterTransition = Slide(GravityCompat.getAbsoluteGravity(GravityCompat.END, resources.configuration.layoutDirection))
        newFragment.exitTransition = Slide(GravityCompat.getAbsoluteGravity(GravityCompat.START, resources.configuration.layoutDirection))
        // Replace whatever is in the fragment_container view with this
        // fragment,
        // and add the transaction to the back stack so the user can navigate
        // back
        transaction.replace(R.id.fragment_container, newFragment, AlbumsFragment.TAG)
        transaction.addToBackStack("DirectoryAlbumsFragment")
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setCheckedItem(R.id.nav_library)

        // Commit the transaction
        transaction.commit()
    }

    fun setNavbarHeader(text: String?) {
        val header = findViewById<TextView>(R.id.navdrawer_header_text) ?: return
        if (text == null) {
            header.text = ""
        }
        header.text = text
    }

    override fun openArtworkSettings() {
        // Create fragment and give it an argument for the selected directory
        val newFragment = ArtworkSettingsFragment.newInstance()
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        newFragment.enterTransition = Slide(GravityCompat.getAbsoluteGravity(GravityCompat.END, resources.configuration.layoutDirection))
        newFragment.exitTransition = Slide(GravityCompat.getAbsoluteGravity(GravityCompat.START, resources.configuration.layoutDirection))
        transaction.addToBackStack("ArtworkSettingsFragment")
        transaction.replace(R.id.fragment_container, newFragment)

        // Commit the transaction
        transaction.commit()
    }

    // Read default view preference
    private val defaultTab: DEFAULTTAB
        private get() {
            // Read default view preference
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
            val defaultView = sharedPref.getString(getString(R.string.pref_start_view_key), getString(R.string.pref_view_default))

            // the default tab for mymusic
            var defaultTab = DEFAULTTAB.FILES
            if (defaultView == getString(R.string.pref_view_my_music_artists_key)) {
                defaultTab = DEFAULTTAB.ARTISTS
            } else if (defaultView == getString(R.string.pref_view_my_music_albums_key)) {
                defaultTab = DEFAULTTAB.ALBUMS
            } else if (defaultView == getString(R.string.pref_view_my_music_files_key)) {
                defaultTab = DEFAULTTAB.FILES
            }
            return defaultTab
        }

    // Read default view preference
    private val defaultViewID:

    // the nav resource id to mark the right item in the nav drawer
            Int
        private get() {
            // Read default view preference
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
            val defaultView = sharedPref.getString(getString(R.string.pref_start_view_key), getString(R.string.pref_view_default))

            // the nav resource id to mark the right item in the nav drawer
            var navId = -1
            if (defaultView == getString(R.string.pref_view_my_music_files_key)) {
                navId = R.id.nav_library
            } else if (defaultView == getString(R.string.pref_view_my_music_artists_key)) {
                navId = R.id.nav_library
            } else if (defaultView == getString(R.string.pref_view_my_music_albums_key)) {
                navId = R.id.nav_library
            } else if (defaultView == getString(R.string.pref_view_playlists_key)) {
                navId = R.id.nav_saved_playlists
            } else if (defaultView == getString(R.string.pref_view_files_key)) {
                navId = R.id.nav_files
            } else if (defaultView == getString(R.string.pref_view_search_key)) {
                navId = R.id.nav_search
            }
            return navId
        }

    companion object {
        private const val TAG = "MainActivity"
        const val MAINACTIVITY_INTENT_EXTRA_REQUESTEDVIEW = "org.malp.requestedview"
        private const val MAINACTIVITY_SAVED_INSTANCE_NOW_PLAYING_DRAG_STATUS = "MainActivity.NowPlayingDragStatus"
        private const val MAINACTIVITY_SAVED_INSTANCE_NOW_PLAYING_VIEW_SWITCHER_CURRENT_VIEW = "MainActivity.NowPlayingViewSwitcherCurrentView"
    }
}